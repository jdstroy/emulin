// ----------------------------------------
//  Version
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/01/23 11:16:50 $ 
//  $Id: Version.java,v 1.14 2000/01/23 11:16:50 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import emulin.*;

class Version {
  static String version = "0.2.13b";
  public static String get_version( ) {
    return( version );
  }
}
