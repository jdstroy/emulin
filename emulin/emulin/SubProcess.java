// ----------------------------------------
//  SubProcess ( $B%M%C%H%o!<%/F~NO4F;kMQ(B )
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/02/11 15:37:40 $ 
//  $Id: SubProcess.java,v 1.9 2000/02/11 15:37:40 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import java.net.*;
import emulin.*;

public class SubProcess extends Thread {
  public static int ACCEPT_WAIT = 0;
  public static int ACCEPT_DONE = 1;
  public static int ACCEPT_MISS = 2;
  static int BUFSIZE = 1024;
  Sysinfo  sysinfo;
  RingBuffer ringbuffer;
  Fileinfo finfo;
  byte buf[];
  int addr_info[];
  int fd;
  boolean opened;
  boolean listen_mode;
  int accept_flag;
  ServerSocket  sconn;
  Socket         conn;


  // $B%5%V%W%m%;%9$N@8@.(B
  public SubProcess( Sysinfo _sysinfo, Fileinfo _finfo, int _fd ) {
    // $B%*%V%8%'%/%H$N@8@.(B
    sysinfo = _sysinfo;
    finfo   = _finfo;
    fd      = _fd;
    buf     = new byte[BUFSIZE];
    ringbuffer = new RingBuffer( _sysinfo, BUFSIZE );
    addr_info = new int[2]; 
    opened = true;
    listen_mode = false;
  }

  // $B%j%C%9%s4F;k%b!<%I$KF~$k(B
  public void set_listen_mode( ServerSocket _sconn ) {
    sconn = _sconn;
    listen_mode = true;
  }
  
  // $B%5!<%P%=%1%C%H%$%s%9%?%s%9$rJV$9(B
  public ServerSocket get_sconn( ) {
    return( sconn );
  }

  // socket$B$+$i$N@hFI$_=hM}(B
  public void run( ) {
    if( listen_mode ) { // LISTEN$B%b!<%I(B
      accept_flag = ACCEPT_WAIT;
      while( true ) {
	if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:top(listen) " ); }
	try { conn = sconn.accept( ); }
	catch ( IOException m ) { accept_flag = ACCEPT_MISS; break; }
        accept_flag = ACCEPT_DONE;
	if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:accept(" + accept_flag + " ) " ); }
      }
    }
    else {
      while( true ) {
	if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:top " ); }
	if( finfo == null ) { opened = false; break; } // $BL58z$J(B fd $B$J$i(B
	if( opened ) {
	  if( finfo.isSTREAM( )) { /* $B%9%H%j!<%`(B */
	    while( ringbuffer.full( )) { // $B%P%C%U%!%U%k$J$i6u$/$^$GBT$D(B
	      try { Thread.sleep( 100L ); }
	      catch( InterruptedException m ) { };
	      Thread.yield( );
	    }
	    read_byte_sub( );
	  }
	  else { /* $B%G!<%?%0%i%`%Q%1%C%H(B */
            if( ringbuffer.empty( )) {
	      synchronized ( finfo ) {
		int len, i;
		if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:read(DGRAM) " ); }
		len = finfo.recvfrom( buf, addr_info );
		if( len == 0 ) { opened = false; }
		else {
		  for( i = 0 ; i < len ; i++ ) {
		    ringbuffer.rw( buf[i], false );
		  }
		}
	      }
	    }
	  }
	}
	if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:before sleep " ); }
	//	try { Thread.sleep( 30L ); }
	//	catch( InterruptedException m ) { };
	Thread.yield( );
	if( sysinfo.verbose( )) {
	  sysinfo.kernel.println( "fd=" + fd + " sub:len = " + ringbuffer.get_size( ) + " (" + Util.hexstr( (int)buf[0] & 0xFF, 2 ) + ") " );
	}
	if( !opened ) {
	  break;
	}
      }
    }
    if( sysinfo.verbose( )) {
      sysinfo.kernel.println( "fd=" + fd + " sub:END" );
    }
  }
  
  // $B%U%!%$%k$r%/%m!<%:$9$k(B
  public void close( ) {
    opened = false;
  }

  // $B$J$K$+%$%Y%s%H$,$"$C$?$+!)(B
  public boolean isEvent( ) {
    boolean ret = false;
    if( sysinfo.verbose( )) {
      sysinfo.kernel.println( "fd=" + fd + " sub.isOPEN( i )    " + isOPEN( ));
      sysinfo.kernel.println( "fd=" + fd + " sub.Available( i ) " + Available( ));
      sysinfo.kernel.println( "fd=" + fd + " sub.isCLOSE( i )   " + this.isCLOSE( ));
    }
    if( isOPEN( ) && Available( )) { ret = true; }
    if( isCLOSE( ))                { ret = true; }
    if( sysinfo.verbose( )) {
      sysinfo.kernel.println( "fd=" + fd + " sub.isEvent( ) " + ret );
    }
    return( ret );
  }

  // $B%*!<%W%sCf$+!)(B
  public boolean isOPEN( ) {
    return( !isCLOSE( ));
  }

  // $B@\B3Aj<j$,%/%m!<%:$7$?$+!)(B
  public boolean isCLOSE( ) {
    return( !opened );
  }

  // $B%"%/%;%W%H$G$-$?$+!)(B
  public int Accepted( ) {
    return( accept_flag );
  }

  // $BFI$_9~$_$G$-$?$+!)(B
  public boolean Available( ) {
    if( sysinfo.verbose( )) {
      sysinfo.kernel.println( "fd=" + fd + " sub:availe = " + ringbuffer.get_size( ) );
    }
    return( ringbuffer.get_size( ) > 0 );
  }

  // $B%=%1%C%H$+$i$NF~NO$r9T$&(B
  int read_byte_top( byte _buf[], boolean subprocess_flag ) {
    int ret = 0;
    ret = read_byte( _buf );
    return( ret );
  }

  // $B%5%V%W%m%;%9$+$i%3!<%k$9$k(B
  int read_byte_sub( ) {
    byte __byte_buf[];
    __byte_buf = new byte[1];
    if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub:read(STREAM) " ); }

    // 1$B%P%$%HFI$_9~$`(B
    if( !ringbuffer.full( )) {
      if( 0 == finfo.Read( __byte_buf )) {
	if( ringbuffer.empty( )) {
	  if( sysinfo.verbose( )) { sysinfo.kernel.println( "fd=" + fd + " sub: peer is closed. " ); }
	  opened = false;
	}
      }
      else { // $BDI5-(B
	ringbuffer.rw( __byte_buf[0], false );
	if( sysinfo.verbose( )) {
	    sysinfo.kernel.println( "fd=" + fd + " sub:len = " + ringbuffer.get_size( ) + "buf[0]=" + __byte_buf[0] ); 
	}
      }
    }
    return( 1 );
  }

  // $B@hFI$_:Q$_$N%G!<%?$rFI$_9~$`(B
  int read_byte( byte _buf[] ) {
    int ret = 0;
    int i;
    int cnt;
    for( cnt = 0 ; ringbuffer.empty( ) ; cnt++ ) { // empty $B$N4VBT$D(B
	if( sysinfo.verbose( )) {
	    if( 0 == ( cnt % 20 )) {
		sysinfo.kernel.println( " read.byte( )  blocking  opened = " + opened ); 
	    }
	}
	Thread.yield( );
	try { Thread.sleep( 100L ); }
	catch( InterruptedException m ) { };
	if( !opened ) return( 0 ); // peer closed.
    }
    ret = ringbuffer.get_size( );
    if( _buf.length < ret ) {
      ret = _buf.length;
    }
    for( i = 0 ; i < ret ; i++ ) {
      _buf[i] = ringbuffer.rw( (byte)0, true );
    }
    return( ret );
  }

  // $B@hFI$_$7$F$$$?%P%$%H$rFI$_9~$s$@(B
  public int read_byte_top( byte _buf[], int _addr_info[], boolean subprocess_flag ) {
    _addr_info[0] = addr_info[0];
    _addr_info[1] = addr_info[1];
    return( read_byte_top( _buf, subprocess_flag ));
  }
}
