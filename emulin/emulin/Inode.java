// ----------------------------------------
//  Inode ( Inode Infomation )
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/04/13 17:39:47 $ 
//  $Id: Inode.java,v 1.15 1999/04/13 17:39:47 kiyoka Exp $
//  Info : 
//   $BK\%/%i%9$N%a%=%C%I$OA4$F(B,$B2>A[%Q%9$r<u$1<h$k(B
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import emulin.*;

public class Inode
{
  // File types.
  static short __S_IFDIR = (short)0x4000;	/* Directory.  */
  static short __S_IFREG = (short)0x8000;	/* Regular file.  */
  // static short S_IRWXU = (short)0x0700;
  static short S_IRUSR = (short)0x0100;
  static short S_IWUSR = (short)0x0080;
  static short S_IXUSR = (short)0x0040;
  // static short S_IRWXG = (short)0x0070;
  static short S_IRGRP = (short)0x0020;
  static short S_IWGRP = (short)0x0010;
  static short S_IXGRP = (short)0x0008;
  // static short S_IRWXO = (short)0x0007;
  static short S_IROTH = (short)0x0004;
  static short S_IWOTH = (short)0x0002;
  static short S_IXOTH = (short)0x0001;

  File  file;

  short st_dev;
  int   st_ino;
  short st_mode;
  short st_nlink;
  short st_uid;
  short st_gid;
  short st_rdev;
  int   st_size;
  int   st_blksize;
  int   st_blocks;
  int   st_atime;
  int   st_mtime;
  int   st_ctime;

  public Inode( String vpath, Sysinfo sysinfo ) {
    String path = sysinfo.get_native_path( vpath );
    file = new File( path );
    //    System.out.println( " Inode.Inode( " + vpath + " , )  path = " + path );
    if( file.exists( )) {
      update_info( vpath, path, sysinfo );
    }
  }

  private boolean update_info( String vpath, String path, Sysinfo sysinfo ) {
    st_dev     = 0;                          // $B%U%!%$%k$,B8:_$9$k%G%P%$%9HV9f(B( $B$J$s$G$b$h$$$O$:(B )
    st_ino     = get_uniq_no( vpath );       // $B%*%s%G%#%9%/(B inode $BHV9f(B( $B%f%K!<%/$G$"$l$P$h$$(B )    
    st_mode    = get_st_mode( vpath );       // $B%U%!%$%k%b!<%I(B
    st_nlink   = 1;                          // $B>o$K(B 1 ( $B%7%s%\%j%C%/%j%s%/$OG'<1$7$J$$(B )
    st_uid     = (short)sysinfo.file_uid( ); // $B%f!<%6!<(B ID
    st_gid     = (short)sysinfo.file_gid( ); // $B%0%k!<%W(B ID
    st_rdev    = 0;                          // $B>o$K(B 0 ( $B%G%P%$%9%U%!%$%k$O07$o$J$$(B )
    st_size    = (int)file.length( );        // $B%U%!%$%k%P%$%H?t(B
    st_blksize = sysinfo.get_block_size( );  // $B%V%m%C%/%5%$%:(B
    st_blocks  = 0;                          // $B8GDj(B
    st_atime   = (int)(file.lastModified( )/1000L);    // $B99?7;~4V(B
    st_mtime   = st_atime;
    st_ctime   = st_mtime;
    return( true );
  }

  // $B%Q%9L>$4$H$K%f%K!<%/$JHV9f$r@8@.$9$k!#(B
  private int get_uniq_no( String pathname ) {
    int i;
    int total = 0;
    String name = new String( pathname );
    //    System.out.println( " orig:get_uniq_no( " + name + " ); " );
    for( i = 0 ; i < name.length( ) ; i++ ) {
      total += (int)name.charAt( i ) + i;
    }
    //    System.out.println( " new :get_uniq_no( " + name + " );  total = " + total );
    return( total );
  }

  private short get_st_mode( String pathname ) {
    short v = 0;
    short rv = (short)(S_IRUSR | S_IRGRP | S_IROTH);
    short wv = (short)(S_IWUSR);
    short xv = (short)(S_IXUSR | S_IXGRP | S_IXOTH);
    // $B%U%!%$%k%?%$%W$N2r@O(B
    if( file.isDirectory( ) ) {
      v |= (short)( __S_IFDIR | xv );
    }
    else {
      if( file.isFile( )) {
	v |= (short)(__S_IFREG | xv );
      }
    }

    // 0x200 ... T
    // 0x400 ... S
    // 0x800 ... S2
    // 0x1000 ... p
    // 0x2000 ... ? $B$J$<$+(B SHRD $BL?Na$,%5%]!<%H$5$l$F$$$J$$$H$$$C$F;_$^$k(B
    // 0x4000 ... $B%G%#%l%/%H%j(B
    // 0x8000 ... $BDL>o%U%!%$%k(B

    // $B%U%!%$%k%Q!<%_%C%7%g%s(B
    if( file.canRead( ))  {  v |= rv; }
    if( file.canWrite( )) {  v |= (short)(rv | wv); }
    return( v );
  }

  // $B%G%#%l%/%H%j%Q!<%_%C%7%g%s$"$j$+!)(B
  public boolean isDirectory( ) {
    boolean ret = false;
    if( 0 != ( st_mode & __S_IFDIR )) {
      ret = true;
    }
    return( ret );
  }

  // $B%j!<%I%Q!<%_%C%7%g%s$"$j$+!)(B
  public boolean isReadable( ) {
    boolean ret = false;
    if( 0 != ( st_mode & S_IRUSR )) {
      ret = true;
    }
    return( ret );
  }

  // $B%j!<%I%Q!<%_%C%7%g%s$"$j$+!)(B
  public boolean isWritable( ) {
    boolean ret = false;
    if( 0 != ( st_mode & S_IWUSR )) {
      ret = true;
    }
    return( ret );
  }

  // $B%U%!%$%k$,B8:_$7$F$$$k$+!)(B
  public boolean isExists( ) {
    return( file.exists( ));
  }
}
