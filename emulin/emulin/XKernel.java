// ----------------------------------------
//  Emulin XKernel
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/05/14 17:06:53 $ 
//  $Id: XKernel.java,v 1.9 1999/05/14 17:06:53 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import java.util.*;
import emulin.*;
import emulin.device.*;

public class XKernel extends Thread {
  Vector  ptable;
  Sysinfo sysinfo;
  Console console;  // $B%3%s%=!<%k%G%P%$%9(B

  // console $B$X$NI=<((B($B2~9T$"$j(B)
  static public void println( String str ) {
    System.out.println( "Kernel : " + str );
  }

  // console $B$X$NI=<((B( 1$B%P%$%H(B)
  public void write( int data ) {
    System.out.write( data );
  }

  // $B?F%W%m%;%9$N(B id $B$r5a$a$k!#(B
  public int search_ppid( Process process ) {
    int i;
    int ppid = 1;
    if( sysinfo.verbose( )) {
      println( " search_ppid( " + process + " );   ptable.size( ) = " + ptable.size( ));
    }
    for( i = 0 ; i < ptable.size( ) ; i++ ) {
      ProcessInfo pinfo = (ProcessInfo)ptable.elementAt( i );
      if( pinfo.process != null ) {
	if( sysinfo.verbose( )) {
	  println( " search_ppid : i = " + i + " pinfo.process = " + pinfo.process );
	}
	if( pinfo.process == process ) {
	  ppid = pinfo.ppid;
	}
      }
    }
    return( ppid );
  }
}
