// ----------------------------------------
//  System Information of Process
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/05/11 17:08:04 $ 
//  $Id: Sysinfo.java,v 1.5 1999/05/11 17:08:04 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import emulin.*;

public class Sysinfo extends Mount {

  public Sysinfo( int __verbose_level, boolean __debug ) {
    _verbose_level  = __verbose_level;
    _debug          = __debug;
    // $B%G%U%)%k%HCM(B
  }

  // $B%3%s%U%#%0%U%!%$%k$rFI$_9~$`(B
  public boolean load_config( String filename ) {
    boolean ret = true;
    RandomAccessFile in;
    String token[] = new String[10];
    String str;
    int len, i;
    try { in = new RandomAccessFile( get_native_path( filename ), "r" ); }
    catch ( IOException m ) { kernel.println( "File open error [" + filename + "]" ); return( false ); }
    for( ; ; ) {
      try { str = in.readLine( ); }
      catch ( IOException m ) { kernel.println( "File read error [" + filename + "]" ); return( false ); }
      if( null == str ) { break; }

      if( verbose( )) {
	kernel.println( "read_line : str = " + str );
      }
      // $B%H!<%/%s$KJ,3d$9$k!#(B
      for( i = 0 ; ; i++ ) {
	str = str.trim( );
	len = str.indexOf( ' ' );
	if( -1 == len ) { token[i] = str; }
	else            { token[i] = new String( str.substring( 0, len )); }
	token[i] = token[i].trim( );
	if( verbose( )) {
	  kernel.println( " token[" + i + "] = " + token[i] );
	}
	if( -1 == len ) { break; }
	else            { str = str.substring( len ); }
      }
      if( 0 == token[0].length( )) continue; // $B6u9T(B
      if( '#' != token[0].charAt( 0 )) { // $B%3%a%s%H0J30$J$i(B
	if( token[0].equals( "root" )) {
          set_root( token[1] );
	}
      }
    }
    return( ret );
  }
}
