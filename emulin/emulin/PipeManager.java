// ----------------------------------------
//  Emulin Pipe Manager
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/05/06 14:57:03 $ 
//  $Id: PipeManager.java,v 1.7 1999/05/06 14:57:03 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import java.util.*;
import emulin.*;

// $BL>A0L5$7%Q%$%W$N>pJs(B
class Pipeinfo {
  static int buf_size = 64*1024;// $B%P%C%U%!%5%$%:(B
  byte buf[];                   // $B%Q%$%WMQ%P%C%U%!(B
  int used;                     // $B;HMQ%P%$%H?t(B
  int wp;                       // $B=PNO$N=q$-9~$_%]%$%s%?(B
  int rp;                       // $BF~NO$NFI$_$@$7%]%$%s%?(B
  int i_connected;              // $B@\B3$5$l$?2s?t(B in
  int o_connected;              // $B@\B3$5$l$?2s?t(B out

  public Pipeinfo( ) {
    buf = new byte[ buf_size ];
    used = 0;
    wp  = 0;
    rp  = 0;
    i_connected  = 1;
    o_connected = 1;
  }

  // $B@\B3$5$l$F$$$k$+!)(B
  public boolean is_connected( ) {
    if( i_connected == 0 || o_connected == 0 ) { return( false ); }
    return( true );
  }

  // $B%j!<%I$7$?%P%$%H?t$rJV$9!#(B
  public int read( byte _buf[] ) {
    int i;
    // Kernel.println( "  Pipeinfo.read( )     rp = " + rp + " wp = " + wp );
    for( i = 0 ; i < _buf.length ; ) {
      if( rp >= buf_size ) { rp = 0; } // $B%P%C%U%!$N%j%s%02=(B 
      while( used <= 0 ) {
	if( !is_connected( )) { return( i ); } // EOF
	// Kernel.println( "  Pipeinfo.read( )    waiting  for ...  i = " + i );
	try { Thread.sleep( 1000L ); }
	catch( InterruptedException m ) { };
	Thread.yield( );
      }
      _buf[i++] = buf[rp++];
      used--;
    }
    // Kernel.println( "  Pipeinfo.read( )     done  i = " + i );
    return( i );
  }

  // $B%i%$%H$7$?%P%$%H?t$rJV$9!#(B
  public boolean write( byte _buf[] ) {
    int i;
    // Kernel.println( "  Pipeinfo.write( )     rp = " + rp + " wp = " + wp );
    if( !is_connected( )) {
      return( false );
    }

    for( i = 0 ; i < _buf.length ; i++ ) {
      if( wp >= buf_size ) { wp = 0; } // $B%P%C%U%!$N%j%s%02=(B 
      while( buf_size <= used ) { // $B%P%C%U%!%U%k$N4VBT$D(B
	if( !is_connected( )) { return( false ); } // $B%Q%$%W$N@ZCG(B
	// Kernel.println( "  Pipeinfo.write( )    waiting  for ...  i = " + i );
	try { Thread.sleep( 1000L ); }
	catch( InterruptedException m ) { };
	Thread.yield( );
      }
      buf[wp++] = _buf[i];
      used++;
    }
    return( true );
  }
}

public class PipeManager extends XKernel {
  Vector pipetable; // $B%Q%$%W%F!<%V%k(B

  public PipeManager( ) {
    pipetable = new Vector( );
  }

  // $B%Q%$%W$r@8@.$9$k!#(B
  // $B@8@.$7$?%Q%$%W$NHV9f$rJV$9!#(B
  public int connect_pipe( ) {
    // $B@8@.(B
    Pipeinfo pipe  = new Pipeinfo( );
    // $B%W%m%;%9$X$N@_Dj(B
    if( sysinfo.verbose( )) {
      println( " connect_pipe( ) : pipe_no = " + pipetable.size( ));
    }
    pipetable.addElement( (Object)pipe );
    disp_pipe( pipetable.size( )-1 );
    return( pipetable.size( )-1 );
  }

  // $B4{$K@\B3$5$l$F$$$k$+D4$Y$k(B
  public boolean is_pipe_connected( int pipe_no ) {
    Pipeinfo pipe = (Pipeinfo)pipetable.elementAt( pipe_no );
    // $BF~NO$^$?$O=PNO$N;2>H?t$,(B 0 $B$J$i@ZCG$5$l$F$$$k!#(B
    return( pipe.is_connected( ));
  }

  // $B%Q%$%W$+$i%j!<%I$9$k!#(B
  public int pipe_read( int pipe_no, byte buf[] ) {
    int len = 0;
    Pipeinfo pipe = (Pipeinfo)pipetable.elementAt( pipe_no );
    int l = 0;

    disp_pipe( pipe_no );

   return( pipe.read( buf ));
  }

  // $B%Q%$%W$X%i%$%H$9$k!#(B
  public boolean pipe_write( int pipe_no, byte buf[] ) {
    Pipeinfo pipe = (Pipeinfo)pipetable.elementAt( pipe_no );

    disp_pipe( pipe_no );

    // $B@ZCG$5$l$F$$$?$i%j!<%I<:GT(B
    if( !is_pipe_connected( pipe_no )) { return( false ); }

    return( pipe.write( buf ));
  }

  // $B%Q%$%W$N@\B3>u67$rI=<($9$k!#(B
  private void disp_pipe( int pipe_no ) {
    int i;
    if( false ) {
      println( "disp_pipe : pipe_no = " + pipe_no  );
      for( i = 0 ; i < pipetable.size( ) ; i++ ) {
	Pipeinfo pipe = (Pipeinfo)pipetable.elementAt( i );
	if( pipe == null ) {
	  println( "    ---- pipe[" + i + "]  is null " );
	}
	else {
	  println( "    ---- pipe[" + i + "]  i_connected = " + pipe.i_connected + " o_connected = " + pipe.o_connected );
	}
      }
    }
  }


  // $B%Q%$%W$r@ZCG$9$k!#(B
  public void disconnect_pipe( int pipe_no, boolean input_flag ) {
    Pipeinfo pipe = null;
    if( pipe_no < 0 )  {return;}

    pipe = (Pipeinfo)pipetable.elementAt( pipe_no );
    if( pipe == null ) {return;}
    if( input_flag ) { pipe.i_connected--; }
    else             { pipe.o_connected--; }
    if( sysinfo.verbose( )) {
      println( " ---- disconnect_pipe( " + pipe_no + " );  i_connected = " + pipe.i_connected + "  o_connected = " + pipe.o_connected );
    }
    disp_pipe( pipe_no );
  }

  // $B%Q%$%W$r(Bduplicate $B$9$k!#(B
  public void duplicate_pipe( int pipe_no, boolean input_flag ) {
    Pipeinfo pipe = (Pipeinfo)pipetable.elementAt( pipe_no );
    if( pipe == null ) {return;}

    if( input_flag ) { pipe.i_connected++; }
    else             { pipe.o_connected++; }
    if( sysinfo.verbose( )) {
      println( " ---- duplicate_pipe( " + pipe_no + " );  i_connected = " + pipe.i_connected + "  o_connected = " + pipe.o_connected );
    }

    disp_pipe( pipe_no );

  }
}
