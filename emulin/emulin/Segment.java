// ----------------------------------------
//  Segment Information in Elf
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/02/03 12:52:38 $ 
//  $Id: Segment.java,v 1.22 2000/02/03 12:52:38 kiyoka Exp $
// ----------------------------------------
package emulin;

//
//typedef struct
//{
//  Elf32_Word	p_type;			/* Segment type */
//  Elf32_Off	p_offset;		/* Segment file offset */
//  Elf32_Addr	p_vaddr;		/* Segment virtual address */
//  Elf32_Addr	p_paddr;		/* Segment physical address */
//  Elf32_Word	p_filesz;		/* Segment size in file */
//  Elf32_Word	p_memsz;		/* Segment size in memory */
//  Elf32_Word	p_flags;		/* Segment flags */
//  Elf32_Word	p_align;		/* Segment alignment */
//} Elf32_Phdr;
//

import java.lang.*;
import java.io.*;
import emulin.*;

public class Segment
{
  static int PF_X	= (1 << 0);	/* Segment is executable */
  static int PF_W	= (1 << 1);	/* Segment is writable */
  static int PF_R	= (1 << 2);	/* Segment is readable */

  int p_type;		/* Segment type */
  int p_offset;		/* Segment file offset */
  int p_vaddr;		/* Segment virtual address */
  int p_paddr;		/* Segment physical address */
  int p_filesz;		/* Segment size in file */
  int p_memsz;		/* Segment size in memory */
  int p_flags;		/* Segment flags */
  int p_align;		/* Segment alignment */
  byte buf[];           /* Segment $B%a%b%j(B */

  boolean bss;          /* BSS$B$r4^$`%;%0%a%s%H$+(B? */

  Sysinfo sysinfo;      /* Process $B%7%9%F%`>pJs(B */
  Process process;      /* Process $B>pJs(B */

  public Segment( Sysinfo _sysinfo, Process _process ) {
    sysinfo = _sysinfo;
    process = _process;
  }

  // $B<+J,$NJ#@=$rJV$9(B
  public Segment duplicate( ) {
    Segment _segment       =   new Segment( sysinfo, process );
    _segment.p_type        =   p_type;
    _segment.p_offset      =   p_offset;
    _segment.p_vaddr       =   p_vaddr;
    _segment.p_paddr       =   p_paddr;
    _segment.p_filesz      =   p_filesz;
    _segment.p_memsz       =   p_memsz;
    _segment.p_flags       =   p_flags;
    _segment.p_align       =   p_align;
    _segment.buf           =   new byte[ buf.length ];
    System.arraycopy( buf, 0, _segment.buf, 0, buf.length );
    _segment.bss           =   bss;
    return( _segment );
  }

  // $B%W%m%0%i%`%X%C%@$N%m!<%I(B
  public boolean load_ph( RandomAccessFile in ) {
    // $B#1%;%0%a%s%HJ,$N%X%C%@>pJs$r%m!<%I$9$k(B
    p_type        =   LoadUtil.little32( in, sysinfo.kernel );
    p_offset      =   LoadUtil.little32( in, sysinfo.kernel );
    p_vaddr       =   LoadUtil.little32( in, sysinfo.kernel );
    p_paddr       =   LoadUtil.little32( in, sysinfo.kernel );
    p_filesz      =   LoadUtil.little32( in, sysinfo.kernel );
    p_memsz       =   LoadUtil.little32( in, sysinfo.kernel );
    p_flags       =   LoadUtil.little32( in, sysinfo.kernel );
    p_align       =   LoadUtil.little32( in, sysinfo.kernel );
    print_segment_info( );
    return( true );
  }

  void print_segment_info( ) {
    if( sysinfo.debug( )) {
      // $B%?%$%W$N2r@O(B
      String t = "";
      if( 0 != (PF_X & p_type)) {  t += "X";  }
      if( 0 != (PF_W & p_type)) {  t += "W";  }
      if( 0 != (PF_R & p_type)) {  t += "R";  }
      process.println( "  ----- Program Segment -----" );
      process.println( "  p_type        : " + Integer.toString( p_type,       16) + "(" + t + ")" );
      process.println( "  p_offset      : " + Integer.toString( p_offset,       16));
      process.println( "  p_vaddr       : " + Integer.toString( p_vaddr,        16));
      process.println( "  p_paddr       : " + Integer.toString( p_paddr,        16));
      process.println( "  p_filesz      : " + Integer.toString( p_filesz,       16));
      process.println( "  p_memsz       : " + Integer.toString( p_memsz,        16));
      process.println( "  p_flags       : " + Integer.toString( p_flags,        16));
      process.println( "  p_align       : " + Integer.toString( p_align,        16));
    }
  }

  // $B%;%0%a%s%H$N:G=*%"%I%l%9$rJV$9(B
  public int segment_end( ) {
    return( p_vaddr + p_memsz );
  }

  public boolean load_body( RandomAccessFile in ) {
    // $B%U%!%$%k$+$i%G!<%?$r%m!<%I$9$k(B
    buf = new byte[p_memsz];   // $B%a%b%j3NJ](B
    try {
      in.seek( p_offset ); 
      in.read( buf, 0, p_filesz );
    }
    catch ( IOException m ) {  process.println( "Seek Failed : offset " + p_offset ); return( false ); }    
    if( sysinfo.debug( )) {
      process.println( "  ----- Segment body loading  address = " + Util.hexstr( p_paddr, 8 ) + " -----" );
    }
    return( true );
  }

  // $B%9%?%C%/%;%0%a%s%H$H$7$F=i4|2=$9$k(B
  public void stack( int stack_size ) {
    String t = "";
    buf = new byte[stack_size];   // $B%a%b%j3NJ](B
    // $B%?%$%W$N@_Dj(B
    p_type = PF_W | PF_R; // R/W $B2DG=(B
    p_vaddr = sysinfo.get_stack_bottom( )-stack_size;
    p_paddr = sysinfo.get_stack_bottom( )-stack_size;
    p_memsz = stack_size;

    if( sysinfo.debug( )) {
      // $B%?%$%W$N2r@O(B
      if( 0 != (PF_X & p_type)) {  t += "X";  }
      if( 0 != (PF_W & p_type)) {  t += "W";  }
      if( 0 != (PF_R & p_type)) {  t += "R";  }
      process.println( "  ----- Stack Segment Inited  size = " + Util.hexstr( stack_size, 6 ) + " -----" );
      process.println( "  ----- Segment (stack) -----" );
      process.println( "  p_type        : " + t );
      process.println( "  p_vaddr       : " + Integer.toString( p_vaddr,        16));
      process.println( "  p_paddr       : " + Integer.toString( p_paddr,        16));
      process.println( "  p_memsz       : " + Integer.toString( p_memsz,        16));
    }
  }

  // $B%;%0%a%s%HCf%G!<%?$NFI$_$@$7(B  (byte)
  public byte peekb( int address ) {
    return( buf[address - p_vaddr] );
  }

  // $B%;%0%a%s%HCf%G!<%?$NFI$_$@$7(B  (bytes)
  public int peekbs( int address, byte _buf[] ) {
    int i;
    int index = address - p_vaddr;
    for( i = 0 ; i < _buf.length ; i++, index++ ) {
      if(( index >= 0)&&(index < buf.length )) { _buf[i] = buf[index]; }
      else{ break; }
    }
    return( i );
  }

  // $B%;%0%a%s%H$X$N%G!<%?=q$-9~$_(B (byte)
  public void pokeb( int address, byte data ) {
    buf[address - p_vaddr] = data;
  }

  // $B%"%I%l%9$,%;%0%a%s%HFb$+$I$&$+D4$Y$k(B
  public boolean in( int address ) {
    if( ( p_vaddr <= address ) && ( address < (p_vaddr + p_memsz)) ) {
      return( true );
    }
    return( false );
  }

  // $B%a%b%j%5%$%:$r(Baddress $B$N%]%$%s%H$^$G3HD%$9$k(B
  public boolean expand_memory( int address ) {
    boolean ret = true;
    int target_size = address - p_vaddr;
    byte tmp_array[] = new byte[target_size]; // $B?7%P%C%U%!3NJ](B
    int i;
    if( sysinfo.verbose( ) ) {
      process.println( "expanded_memory( ):target_size = " + Util.hexstr( target_size, 8 ));
      process.println( "expanded_memory( ):p_memsz     = " + Util.hexstr( p_memsz, 8 ));
    }
    if( p_memsz > target_size ) { // $B%5%$%:$N=L>.(B
      p_memsz = target_size;
    }
    for( i = 0 ; i < p_memsz ; i++ ) {  // $B?7%P%C%U%!$X$N%3%T!<(B
      tmp_array[i] = buf[i];
    }
    buf = tmp_array;  // $B?7%P%C%U%!$KCV$-49$($k(B
    p_memsz = target_size;
    print_segment_info( );
    return( ret );
  }
}
