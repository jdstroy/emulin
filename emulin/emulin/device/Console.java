// ----------------------------------------
//  Console
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/01/13 15:50:42 $ 
//  $Id: Console.java,v 1.6 2000/01/13 15:50:42 kiyoka Exp $
// ----------------------------------------
package emulin.device;

import java.io.*;
import java.lang.*;
import java.util.*;
import emulin.*;

public class Console extends NativeConsole {

  public Console( Sysinfo _sysinfo ) {
    sysinfo = _sysinfo;
    Native_init( _sysinfo );
  }

  // $B%3%s%=!<%k$+$i$N%j!<%I(B
  public int read( byte buf[], emulin.Process _process ) {
    int len = 0;
    if( sysinfo.is_console_none( ))    {  len = Std_read(    buf, _process ); }
    if( sysinfo.is_console_native( ))  {  len = Native_read( buf, _process ); }
    return( len );
  }

  // $B%3%s%=!<%k$X$N%i%$%H(B
  public int write( byte buf[], boolean stderr_flag ) {
    if( sysinfo.is_console_none( ))    {  Std_write( buf, stderr_flag ); }
    if( sysinfo.is_console_native( ))  {  Std_write( buf, stderr_flag ); }
    return( buf.length );
  }

  // $B%Q%i%a!<%?$NJQ99(B
  public boolean set_parameter( int c_lflag, int c_iflag, int c_oflag, byte c_cc[] ) {
    if( sysinfo.is_console_native( ))  {  Native_set_parameter( c_lflag, c_iflag, c_oflag, c_cc ); }
    return( true );
  }

  // $B3d$j9~$_$N%A%'%C%/(B
  public boolean check_int( ) {
      boolean ret = false;
      if( sysinfo.is_console_none( ))    {  ret = Std_check_int( ); }
      if( sysinfo.is_console_native( ))  {  ret = Native_check_int( ); }
      return( ret );
  }

  // $B3d$j9~$_$N%-%c%s%;%k(B
  public void cancel_int( ) {
      if( sysinfo.is_console_none( ))    {  Std_cancel_int( ); }
      if( sysinfo.is_console_native( ))  {  Native_cancel_int( ); }
  }

  // $B3d$j9~$_$,(B 1$B2sF~$C$?$3$H$K$9$k!#(B
  public void set_int( int sig ) {
      if( sysinfo.is_console_none( ))    {  Std_set_int( sig ); }
      if( sysinfo.is_console_native( ))  {  Native_set_int( sig ); }
  }
}
