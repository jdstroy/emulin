// ----------------------------------------
//  Native Console ( using JNI )
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/01/13 15:50:47 $ 
//  $Id: NativeConsole.java,v 1.10 2000/01/13 15:50:47 kiyoka Exp $
// ----------------------------------------
package emulin.device;

import java.io.*;
import java.lang.*;
import java.util.*;
import emulin.*;
import emulin.RootSysinfo.*;
import emulin.Sysinfo.*;
import emulin.Signal.*;

public class NativeConsole extends StdConsole {
  static int IGNBRK	= 0x0000001;
  static int BRKINT	= 0x0000002;
  static int IGNPAR	= 0x0000004;
  static int PARMRK	= 0x0000010;
  static int INPCK	= 0x0000020;
  static int ISTRIP	= 0x0000040;
  static int INLCR	= 0x0000100;
  static int IGNCR	= 0x0000200;
  static int ICRNL	= 0x0000400;
  static int IUCLC	= 0x0001000;
  static int IXON	= 0x0002000;
  static int IXANY	= 0x0004000;
  static int IXOFF	= 0x0010000;
  static int IMAXBEL	= 0x0020000;

  static int VMIN  = 6;
  static int VTIME = 5;
  int vmin;
  int vtime;
  int byte_buf;
  int iflag;
  int oflag;
  boolean int_flag;

  public void Native_init( Sysinfo _sysinfo ) {
    int_flag = false;
    vmin  = 0;
    vtime = 0;
    byte_buf = 0;
    iflag = ICRNL | INLCR;
    oflag = 0;
    if( _sysinfo.CONSOLE_NATIVE == _sysinfo.get_console_type( )) {
      System.out.println( "Info:native console library installed..." );
      System.loadLibrary("emu_con");
      native_init( );
    }
  }

  // Native
  public native int native_init( );
  public static native int native_read( );
  public native int native_set_parameter( int c_lflag, byte c_cc );
  public native int native_israw( );
  public native int native_check_int( );
  public native int native_cancel_int( );
  public native int native_set_int( int sig );

  // $B%3%s%=!<%k$+$i$NFI$_9~$_(B
  public int Native_read( byte buf[], emulin.Process _process ) {
    int i;
    int b = -1;
    int raw = native_israw( );
    if( false ) {
      System.out.println(  "native_israw = " + raw );
      System.out.println(  "vmin  = " + vmin );
      System.out.println(  "vtime = " + vtime );
    }
    for( i = 0 ; i < buf.length ; ) {
      int sig;
      //      System.out.println( " iflag = " + Util.hexstr( iflag, 8 ));
      do {
	try { Thread.sleep( 20L ); }
	catch( InterruptedException m ) { };
	Thread.yield( );

	_int_check_and_send( _process.sysinfo );

	// signal $B$r<u$1$?>l9g$O$$$C$?$sCfCG$9$k!#(B
	sig = _process.psig( );
	// System.out.println( "deb(1) sig=(" + sig + ")" );
	if( -1 != sig ) { return( i ); }
	// System.out.println( "deb(2) sig=(" + sig + ")" );

      } while( sysinfo.console_buf == 0 );

      b = sysinfo.console_buf;  if( 0xA == b ) { b = 0xD; } // $BJQ49$9$k(B
      sysinfo.console_buf = 0;

      // System.out.println( "[" + Util.hexstr( b, 8 ) + "]" );
      if( -1 == b ) { return( i ); }
      else {

        if( 0x4 == b ) {
	}
	else {
	  if( 0 != (iflag & IGNCR) ) {  if( 0xA == b ) { b = -1;  }}
	  if( 0 != (iflag & ICRNL) ) {  if( 0xA == b ) { b = 0xD; }}
	  if( 0 != (iflag & INLCR) ) {  if( 0xD == b ) { b = 0xA; }}
	  if( b >= 0 ) {
	    buf[i] = (byte)b; i++;
	  }
	}
      }
      if( b >= 0 ) {
	if( 0 != raw ) { break; }
	if( 0xA == b ) { break; }  // $B2~9T$G=hM}$r=*$j$K$9$k!#(B
	if( 0x4 == b ) { break; }  // EOF$B$G=hM}$r=*$j$K$9$k!#(B
      }
    }
    return( i );
  }

  // $BF~NO$,$?$^$C$F$$$k$+$I$&$+D4$Y$k!#(B
  public boolean Available( ) {
    return( sysinfo.console_buf > 0 );
  }

  // $BF~NO$,$"$l$P(B,$BJV$9(B
  public int _byte_read( Sysinfo _sysinfo ) {
      if( _sysinfo.console_buf == 0 ) {
	  _sysinfo.console_buf = native_read( );
      }
      return( _sysinfo.console_buf );
  }

  // $B3d$j9~$_$r%A%'%C%/$7$F(B,$B%7%0%J%k$rF~$l$k(B
  public synchronized void _int_check_and_send( Sysinfo _sysinfo ) {
      if( Native_check_int( )) { // STDIN$B$r;}$D%W%m%;%9$K(BSIGINT$B$rAw$k(B
	  Native_cancel_int( );
	  _sysinfo.kernel.kill( -1, Signal.SIGINT );
      }
      if( _sysinfo.verbose( )) {
	  _sysinfo.kernel.println( "[init process]:check_int( ) " );
      }
  }

  // $B%Q%i%a!<%?$N@_Dj(B
  public void Native_set_parameter( int c_lflag, int c_iflag, int c_oflag, byte c_cc[] ) {
    vmin  = c_cc[VMIN];
    vtime = c_cc[VTIME];
    iflag = c_iflag;
    oflag = c_oflag;
    native_set_parameter( c_lflag, c_cc[VMIN] );
  }

  // $B3d$j9~$_$N%A%'%C%/(B
  public boolean Native_check_int( ) {
      if( 0 < native_check_int( )) {
	  return( true );
      }
      return( false );
  }

  // $B3d$j9~$_$N%-%c%s%;%k(B
  public void Native_cancel_int( ) {
      native_cancel_int( );
  }

  // $B3d$j9~$_$,(B 1$B2sF~$C$?$3$H$K$9$k!#(B
  public void Native_set_int( int sig ) {
      // System.out.println( "Native_set_int( )" );
      native_set_int( sig );
  }
}
