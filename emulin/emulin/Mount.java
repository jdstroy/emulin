// ----------------------------------------
//  FileSystem Mount Manager
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/11/30 15:35:58 $ 
//  $Id: Mount.java,v 1.15 1999/11/30 15:35:58 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import java.util.*;
import emulin.*;

class MountInfo {
  String _virtual;
  String _native;

  MountInfo( String __virtual, String __native ) {
    _virtual = __virtual; // $B2>A[%^%&%s%H%]%$%s%H(B
    _native  = __native;  // $B%^%&%s%H%Q%9(B
  }
}

// $B%^%&%s%H%]%$%s%H$N4IM}$r9T$&(B
public class Mount extends RootSysinfo {
  Vector mounts;      // $B%^%&%s%H%]%$%s%H(B
  String root;        // $B%k!<%H%]%$%s%H(B
  String native_sep;  // $B$=$N(BOS$B$N%U%!%$%k%;%Q%l!<%?(B

  Mount( ) {
    mounts = new Vector( );
    native_sep = System.getProperty( "file.separator" );
  }

  // $B%k!<%H%]%$%s%H$r@_Dj$9$k(B
  public void set_root( String _root ) {
    int index = 0;
    if( native_sep.charAt( 0 ) == _root.charAt( _root.length( ) -1 )) { index = 1; }
    root = new String( _root.substring( 0, _root.length( ) -index ));
    if( verbose( )) {  kernel.println( " root = " + root );    }
  }

  // $B%^%&%s%H%]%$%s%H$rDI2C$9$k!#(B
  void add_mountpoint( String _mountpoint, String _nativepath ) {
    MountInfo mountinfo = new MountInfo( _mountpoint, _nativepath );
    mounts.addElement( (Object)mountinfo );
  }

  // $B%^%&%s%H%]%$%s%H$rDI2C$9$k!#(B
  void remove_mountpoint( String _name ) {
    int i;
    // mount$B%]%$%s%H$+(Bnative$B%Q%9$K(B _name$B$,%^%C%A$7$?$i$=$N%(%s%H%j$r30$9(B
    for( i = 0 ; i < mounts.size( ) ; i++ ) {
      int len;
      MountInfo mountinfo = (MountInfo)mounts.elementAt( i );
      len = _name.indexOf( mountinfo._native );
      if( 0 == len ) {mounts.removeElementAt( i );break;}
      len = _name.indexOf( mountinfo._virtual );
      if( 0 == len ) {mounts.removeElementAt( i );break;}
    }
  }

  // Native$B%Q%9$+$i2>A[%Q%9$KJQ49$9$k(B
  String get_virtual_path( String _native_path ) {
    int len, no = -1;
    String ret = null;
    String _root = root;
    if( '<' == _native_path.charAt( 0 )) {
      return( _native_path );
    }
    // $B%k!<%H%]%$%s%H$+$i$N%^%C%A%s%0(B
    len = _native_path.indexOf( _root );
    // Mount$B%]%$%s%H$+$i$N%^%C%A%s%0(B
    if( -1 == len ) {
      int i;
      // mount$B%]%$%s%H;XDj$K%^%C%A$7$?>l9g$=$N%Q%9$rJV$9!#(B
      for( i = 0 ; i < mounts.size( ) ; i++ ) {
	MountInfo mountinfo = (MountInfo)mounts.elementAt( i );
	len = _native_path.indexOf( mountinfo._native );
	if( 0 == len ) { // $B%^%C%A$7$?(B
	  no = i;
	  ret = _native_path.substring( mountinfo._native.length( ));
	  ret = mountinfo._virtual + ret;
	  break;
	}
      }
    } else {
      ret = _native_path.substring( _root.length( ));
    }
    if( -1 == len ) {
      System.err.println( "Emulin error : current path is out of virtual path area [" + _native_path + "]" );
      System.exit( 1 );
    }
    ret = ret.replace( native_sep.charAt( 0 ), '/' );
    if( verbose( )) {
      kernel.println( " get_virtual_path( " + _native_path + " )" );
      kernel.println( "   virtual_path = " + ret );
    }
    return( ret );
  }

  // $B2>A[%^%&%s%H%]%$%s%H$+$i(BNative$B%Q%9$KJQ49$9$k(B
  String get_native_path( String _virtual_path ) {
    int i;
    String ret = null;
    int index;
    int len, no = -1;
    String _root = root;
    if( '<' == _virtual_path.charAt( 0 )) { // $BFbIt%U%!%$%k%Q%9$J$N$G(B,$B$J$K$b$7$J$$(B
      return( _virtual_path );
    }
    if( verbose( )) {
      kernel.println( " get_native_path( " + _virtual_path + " )" );
    }
    ret = _root + _virtual_path;
    if( true ) {
      // mount$B%]%$%s%H;XDj$K%^%C%A$7$?>l9g$=$N%Q%9$rJV$9!#(B
      for( i = 0 ; i < mounts.size( ) ; i++ ) {
	MountInfo mountinfo = (MountInfo)mounts.elementAt( i );
	index = _virtual_path.indexOf( mountinfo._virtual );
	if( 0 == index ) { // $B%^%C%A$7$?(B
	  no = i;
	  _root = mountinfo._native;
	  ret = _virtual_path.substring( mountinfo._virtual.length( ));
	  ret = _root + ret;
	  break;
	}
      }
    }
    // $B%k!<%H%]%$%s%H$r(BNative$B%Q%9$K=q$-49$($k(B
    ret = ret.replace( '/', native_sep.charAt( 0 ));
    if( verbose( )) {
      kernel.println( "   native_path( " + no +  " ) = " + ret );
    }
    return ret;
  }

  // $B%U%k%Q%9L>$rJV$9(B
  //   _curdir $B$O2>A[%Q%9(B
  public String get_full_path( String _curdir, String name ) {
    if( 0 == name.length( )) {
      name = _curdir;
      return( name );
    }
    if( '<' == name.charAt( 0 )) {
      return( name );
    }
    if( verbose( )) {
      kernel.println( " get_full_path( " + name + " )" );
    }
    if( '/' != name.charAt( 0 )) {
      name = _curdir + "/" + name;
    }
    if( verbose( )) {
      kernel.println( "   full_path = " + name );
    }
    name = Util.realname( name );
    return( name );
  }
}
