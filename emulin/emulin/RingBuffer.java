// ----------------------------------------
//  RingBuffer ( $B%j%s%0%P%C%U%!(B )
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/02/10 18:59:41 $ 
//  $Id: RingBuffer.java,v 1.2 2000/02/10 18:59:41 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import java.net.*;
import emulin.*;

public class RingBuffer {
  int  bufsize;
  byte buf[];
  int  rp;
  int  wp;
  int  use;

  // $B%P%C%U%!%$%s%9%?%s%9$N@8@.(B
  public RingBuffer( Sysinfo _sysinfo, int size ) {
    // $B%*%V%8%'%/%H$N@8@.(B
    bufsize = size;
    buf     = new byte[size];
    rp      = 0;
    wp      = 0;
    use     = 0;
  }
    
  // $B%5!<%P%=%1%C%H%$%s%9%?%s%9$rJV$9(B
  synchronized public byte rw( byte data, boolean read_flag ) {
    byte ret = 0;
    if( read_flag ) {
      if( use > 0 ) {
          ret = buf[rp++];
	  use--;
	  if( rp >= bufsize ) { rp = 0; }
      }
    }
    else {
      if( use < bufsize ) {
          buf[wp++] = data;
	  use++;
	  if( wp >= bufsize ) { wp = 0; }
      }
    }
    return( ret );
  }

  // $B$?$^$C$F$$$k%P%$%H?t$rJV$9(B
  synchronized public int get_size( ) {
    return( use );
  }

  // FULL $B$+$I$&$+D4$Y$k(B
  synchronized public boolean full( ) {
    return( use == bufsize );
  }

  // EMPTY $B$+$I$&$+D4$Y$k(B
  synchronized public boolean empty( ) {
    return( 0 == use );
  }
}
