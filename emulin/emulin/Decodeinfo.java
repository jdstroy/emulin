// ----------------------------------------
//  IA-32 Cpu Decode info 
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/09/14 16:55:23 $ 
//  $Id: Decodeinfo.java,v 1.2 1999/09/14 16:55:23 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import emulin.*;

public class Decodeinfo
{
  int inst_id;             // $BL?Na(B ID
  int inst_index;          // $BL?Na%G!<%?G[Ns$X$N(B index
  int inst_len;            // $BL?Na%G!<%?%P%$%H?t(B
  boolean o16_flag;	   // $B%*%Z%i%s%I(B16bit$B%U%i%0(B
  boolean a16_flag;        // $B%"%I%l%9(B16bit$B%U%i%0(B
  boolean repnz_flag;      // repne $B%U%i%0(B
  boolean repz_flag;       // repe $B%U%i%0(B
  boolean d_flag;          // dist flag
  boolean w_flag;          // width flag
  boolean W_flag;          // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
  boolean s_flag;          // $BId9f3HD%%U%i%0(B
  boolean r_flag;          // $B%l%8%9%?%U%i%0(B
  boolean c_flag;          // $B>r7o(B
  boolean D_flag;          // $B>r7o(B
  int d_val;               // dist flag
  int w_val;               // width flag
  int W_val;               // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
  int s_val;               // $BId9f3HD%%U%i%0(B
  int r_val;               // $B%l%8%9%?%U%i%0(B
  int c_val;               // $B>r7o(B
  int D_val;               // double$B%U%i%0(B
  int d_index;             // dist flag
  int w_index;             // width flag
  int W_index;             // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
  int s_index;             // $BId9f3HD%%U%i%0(B
  int r_index;             // $B%l%8%9%?%U%i%0(B
  int c_index;             // $B>r7o(B
  int D_index;             // double$B%U%i%0(B

  // $B%"%I%l%C%7%s%0%b!<%I%P%$%H$NCM(B
  int mod;                 // mod $B%U%#!<%k%I(B
  int reg;                 // reg $B%U%#!<%k%I(B
  int rpm;                 // r/m $B%U%#!<%k%I(B

  // SIB$B$NCM(B
  int scale;               // scale $B%U%#!<%k%I(B
  int index;               // index $B%U%#!<%k%I(B
  int base;                // base  $B%U%#!<%k%I(B

  Operand src;             // $B%=!<%9%*%Z%i%s%I(B
  Operand dst;             // $B%G%9%H%*%Z%i%s%I(B
  Operand fst;             // 1$BHVL\$N%*%Z%i%s%I(B( 2$B%*%Z%i%s%I$N;~$O!"L$;HMQ$H$J$k(B )


  public Decodeinfo duplicate( ) {
    Decodeinfo _ret = new Decodeinfo( );

    _ret.inst_id    =     inst_id;             // $BL?Na(B ID
    _ret.inst_index =     inst_index;          // $BL?Na%G!<%?G[Ns$X$N(B index
    _ret.inst_len   =     inst_len;            // $BL?Na%G!<%?%P%$%H?t(B
    _ret.o16_flag   =     o16_flag;	       // $B%*%Z%i%s%I(B16bit$B%U%i%0(B
    _ret.a16_flag   =     a16_flag;            // $B%"%I%l%9(B16bit$B%U%i%0(B
    _ret.repnz_flag =     repnz_flag;          // repne $B%U%i%0(B
    _ret.repz_flag  =     repz_flag;           // repe $B%U%i%0(B
    _ret.d_flag     =     d_flag;              // dist flag
    _ret.w_flag     =     w_flag;              // width flag
    _ret.W_flag     =     W_flag;              // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
    _ret.s_flag     =     s_flag;              // $BId9f3HD%%U%i%0(B
    _ret.r_flag     =     r_flag;              // $B%l%8%9%?%U%i%0(B
    _ret.c_flag     =     c_flag;              // $B>r7o(B
    _ret.D_flag     =     D_flag;              // $B>r7o(B
    _ret.d_val      =     d_val;               // dist flag
    _ret.w_val      =     w_val;               // width flag
    _ret.W_val      =     W_val;               // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
    _ret.s_val      =     s_val;               // $BId9f3HD%%U%i%0(B
    _ret.r_val      =     r_val;               // $B%l%8%9%?%U%i%0(B
    _ret.c_val      =     c_val;               // $B>r7o(B
    _ret.D_val      =     D_val;               // double$B%U%i%0(B
    _ret.d_index    =     d_index;             // dist flag
    _ret.w_index    =     w_index;             // width flag
    _ret.W_index    =     W_index;             // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
    _ret.s_index    =     s_index;             // $BId9f3HD%%U%i%0(B
    _ret.r_index    =     r_index;             // $B%l%8%9%?%U%i%0(B
    _ret.c_index    =     c_index;             // $B>r7o(B
    _ret.D_index    =     D_index;             // double$B%U%i%0(B
    _ret.mod        =     mod;                 // mod $B%U%#!<%k%I(B
    _ret.reg        =     reg;                 // reg $B%U%#!<%k%I(B
    _ret.rpm        =     rpm;                 // r/m $B%U%#!<%k%I(B
    _ret.scale      =     scale;               // scale $B%U%#!<%k%I(B
    _ret.index      =     index;               // index $B%U%#!<%k%I(B
    _ret.base       =     base;                // base  $B%U%#!<%k%I(B
    _ret.src        =     src.duplicate( );    // $B%=!<%9%*%Z%i%s%I(B
    _ret.dst        =     dst.duplicate( );    // $B%G%9%H%*%Z%i%s%I(B
    _ret.fst        =     fst.duplicate( );    // 1$BHVL\$N%*%Z%i%s%I(B( 2$B%*%Z%i%s%I$N;~$O!"L$;HMQ$H$J$k(B )
    return( _ret );
  }
}

