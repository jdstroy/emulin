// ----------------------------------------
//  File Information
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/02/10 18:59:40 $ 
//  $Id: Fileinfo.java,v 1.29 2000/02/10 18:59:40 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import java.net.*;
import emulin.*;

public class Fileinfo
{
  int opened;
  String name;
  RandomAccessFile f;
  String mode;
  int ptr;
  int mode_bit;
  int c_iflag;
  int c_oflag;
  int c_cflag;
  int c_lflag;
  byte c_line;
  byte c_cc[];
  boolean std_flag;
  boolean stderr_flag;
  boolean pipe_in_flag;
  boolean pipe_out_flag;
  int pipe_no;
  boolean socket_flag;
  boolean stream_flag;
  String  ip_str;
  int     ip;
  int     port;
  int     back_log;
  Socket         conn;
  ServerSocket   sconn;
  DatagramSocket dgram;
  SubProcess     subprocess;

  Fileinfo( ) {
    opened = 0;
    c_cc = new byte[19];
    c_iflag = 0x500;
    c_oflag = 0x01;
    c_cflag = 0xBF;
    c_lflag = 0x8A3B;
    c_line = (byte)0;
    c_cc[ 0] =  (byte)0x03;    c_cc[ 1] = (byte)0x1C;    c_cc[ 2] =  (byte)0x08;
    c_cc[ 3] =  (byte)0x00;    c_cc[ 4] = (byte)0x04;
    c_cc[ 5] =  (byte)0x00;    c_cc[ 6] = (byte)0x01;    c_cc[ 7] =  (byte)0x00;
    ptr = 0;
    std_flag    = false;
    stderr_flag = false;
    pipe_in_flag  = false;
    pipe_out_flag = false;
    socket_flag   = false;
    pipe_no       = -1;
    socket_flag   = false;
    stream_flag   = false;
    conn          = null;
    sconn         = null;
    dgram         = null;
    subprocess    = null;
  }
  
  // $BJ#@=(B
  public Fileinfo duplicate( ) {
    Fileinfo _finfo = new Fileinfo( );
    _finfo.opened = opened;
    _finfo.ptr    = ptr;
    _finfo.std_flag       = std_flag;
    _finfo.stderr_flag    = stderr_flag;
    _finfo.pipe_in_flag   = pipe_in_flag;
    _finfo.pipe_out_flag  = pipe_out_flag;
    _finfo.pipe_no        = pipe_no;
    _finfo.socket_flag    = socket_flag;
    _finfo.stream_flag    = stream_flag;
    _finfo.conn           = conn;
    _finfo.subprocess     = subprocess;
    return( _finfo );
  }

  // $B%9%H%j!<%`%=%1%C%H$+%G!<%?%0%i%`%=%1%C%H$+$r;XDj$9$k(B
  public void set_socket_type( boolean _stream_flag ) {
    stream_flag = _stream_flag;
  }

  // $B%/%i%$%"%s%H%=%1%C%H$H$7$F=i4|2=$9$k!#(B
  public boolean client_socket( int _ip, int _port ) {
    boolean  ret = true;
    ip_str = Util.ip_str( Util.swap32( _ip ));
    ip     = _ip;
    port   = _port;
    if( stream_flag ) {
      try { conn = new Socket( ip_str, port, stream_flag ); }
      catch ( IOException m ) { ret = false; }
      {
	  //	  boolean val = false;
	  //	  int error_flag = 0;
	  //	  try { val = conn.getTcpNoDelay( ); }
	  //	  catch ( IOException m ) { error_flag = 1; }
	  //	  try { conn.setTcpNoDelay( true ); }
	  //	  catch ( IOException m ) { error_flag = 2; }
	  //	  try { conn.setSendBufferSize( 1 ); }
	  //	  catch ( IOException m ) { error_flag = 2; }
	  //	  System.out.println( " getTcpNoDelay( ) = " + val + " error flag = " + error_flag );
      }
    }
    else {
      //      System.out.println( "DEBUG: 1  ip_str = " + ip_str );
      //      System.out.println( "DEBUG: 2  port = " + port );
    }
    return( ret );
  }

  // $B%5!<%P!<%=%1%C%H$r:n@.$9$k!#(B
  public boolean make_server_socket( int port ) {
    if( stream_flag ) {
      try { sconn = new ServerSocket( port, back_log ); }
      catch ( IOException m ) { return( false ); }
    }
    else {
      if( port < 0 ) {        
	try { dgram = new DatagramSocket( );  }
	catch ( SocketException m ) { return( false ); }
      }
      else { 
	dgram.close( );
	try { dgram = new DatagramSocket( port ); }
	catch ( SocketException m ) { return( false ); }
      }
    }
    return( true );
  }

  // $B%5!<%P!<%=%1%C%H%$%s%9%?%s%9$r%;%C%H$9$k(B
  public void set_server_socket( Socket _conn ) {
    conn = _conn;
  }

  // $B%5!<%P!<%=%1%C%H$H$7$F=i4|2=$9$k!#(B
  //  public boolean server_socket( ) {
  //    boolean  ret = true;
  //    try { conn = sconn.accept( ); }
  //    catch ( IOException m ) { ret = false; }
  //    return( ret );
  //  }

  // $B%5!<%P!<%=%1%C%H%$%s%9%?%s%9$rJV$9!#(B
  public ServerSocket get_sconn( ) {
    return( sconn );
  }

  // $B%=%1%C%H$,@\B3:Q$+!)(B
  public boolean is_connected( ) {
    boolean ret = true;
    if( conn == null ) { ret = false; }
    else {
      if( false ) { /* $B$3$l$G$O@\B3@h$,%/%m!<%:$5$l$?$N$r8!=P$G$-$J$$(B */
	int len = 0;
	InputStream s = null;
	byte buf[] = new byte[1];
	try{ s =  conn.getInputStream( ); }
	catch ( IOException m ) { return( false ); }
	s.mark( 1 );
	try{ len = s.read( buf ); }
	catch ( IOException m ) { ret = false; }
	try{ s.reset( ); }
	catch ( IOException m ) { ret = false; }
	if( len < 0 ) {
	  ret = false;
	}
	//	System.out.println( "DEBUG: available  len = " + len );
      }
    }
    return( ret );
  }

  // $B%U%!%$%k$NJ#@=(B
  void duplicate_file( Sysinfo sysinfo ) {
    opened++;
    if( sysinfo.verbose( )) {
      sysinfo.kernel.println( " Fileinfo.duplicate_file( )   opened = " + opened );
    }
  }

  // $B%j!<%I(B
  public int Read( byte[] buf ) {
    int ret = 0;
    InputStream s = null;
    if( isSOCKET( )) {
      if( stream_flag ) {
	if( null == conn ) { return( -1 ); }
	else {
	  try{ s =  conn.getInputStream( ); }
	  catch ( IOException m ) { ret = -1; return( ret ); }
	}
	try{ ret = s.read( buf ); }
	catch ( IOException m ) { ret = 0; return( ret ); }
	if( ret == -1 ) { ret = 0; }
	}
      else {
	//	System.out.println( " Fileinfo.Read( read from dgram socket ) " );
      }
    }
    else {
      try{ ret = f.read( buf ); }
      catch ( IOException m ) { ret = -1; return( ret ); }
      if( ret == -1 ) { ret = 0; }
    }      
    return( ret );
  }

  // $B%i%$%H(B
  public boolean Write( byte[] buf ) {
    boolean ret = true;
    OutputStream s = null;
    if( isSOCKET( )) {
      if( stream_flag ) {
	if( null ==  conn ) { return( false ); }
	else {
	  try{ s =  conn.getOutputStream( ); }
	  catch ( IOException m ) { return( false ); }
	}
	//	System.out.println( " Fileinfo.Write( STREAM ) " );
	try{ s.write( buf ); }
	catch ( IOException m ) { ret = false; }
      }
      else {
	//	System.out.println( " Fileinfo.Write( DGRAM ) " );
	ret = sendto( buf );
      }
    }
    else {
      try{ f.write( buf ); }
      catch ( IOException m ) { ret = false; }
    }      
    return( ret );
  }

  // $BF~NO$K$J$s$i$+$N%$%Y%s%H$,$"$C$?$+!)(B
  public boolean isEvent( ) {
    if( null == subprocess ) {
      return( false );
    }
    else {
      return( subprocess.isEvent( ));
    }
  }

  // $B%U%!%$%k$,%*!<%W%s$5$l$F$$$k$+!)(B
  public boolean isOPEN( ) {
    if( null == subprocess ) {
      return( opened > 0 );
    }
    else {
      return( subprocess.isOPEN( ));
    }
  }

  // $B%U%!%$%k$,%/%m!<%:$5$l$F$$$k$+!)(B
  public boolean isCLOSE( ) {
    if( null == subprocess ) {
      return( opened <= 0 );
    }
    else {
      return( subprocess.isCLOSE( ));
    }
  }

  // $B%U%!%$%k$,%/%m!<%:$5$l$F$$$k$+!)(B
  public boolean Available( ) {
    if( null == subprocess ) {
      return( false );
    }
    else {
      return( subprocess.Available( ));
    }
  }

  public int get_mode_bit( ) {
    return( mode_bit );
  }

  public boolean isSTD( ) {
    return( std_flag );
  }

  public boolean isERR( ) {
    return( stderr_flag );
  }

  public void set_ptr( int _ptr ) {
    ptr = _ptr;
  }

  public int get_ptr( ) {
    return( ptr );
  }

  public boolean opendir( String _name ) {
    boolean ret = true;
    name = _name;
    opened = 1;
    return( ret );
  }

  public boolean open( String _name, String mode, int _mode_bit ) {
    boolean ret = true;
    name = _name;
    opened = 1;
    File file;
    mode_bit = _mode_bit;
    if( _name.equals( "<std>" )) { // $BI8=`F~=PNO(B
      std_flag = true;
      return( ret );
    }
    if( _name.equals( "<err>" )) { // $B%(%i!<F~=PNO(B
      stderr_flag = true;
      return( ret );
    }
    if( _name.equals( "<pipe>" )) { // $B%Q%$%W(B
      if( mode.equals( "r" )) { // $B%j!<%I(B
	pipe_in_flag = true;
      }
      else {    // $B%i%$%H(B
	pipe_out_flag = true;
      }
      return( ret );
    }
    if( _name.equals( "<sock>" )) { // $B%=%1%C%H(B
      socket_flag = true;
      return( ret );
    }
    // $B$=$l0J30$N%U%!%$%k(B
    // $B%U%!%$%k$r:o=|$9$k!#(B
    if( mode.equals( "rw" )) { // $B=q$-9~$_%b!<%I$J$i(B
      if( 0 != ( _mode_bit & Syscall.O_TRUNC )) {
	file = new File( _name );
	file.delete( );
      }
    }
    // $B%U%!%$%k$r%*!<%W%s$9$k!#(B
    try { f = new RandomAccessFile( _name, mode ); }
    catch ( IOException m ) {  ret = false; opened = 0; }
    return( ret );
  }

  public boolean close( Sysinfo sysinfo ) {
    boolean ret = true;
    opened--;
    if( subprocess != null ) {
      subprocess.close( );
      subprocess.stop( );
    }
    if( opened < 1 ) {
      if( is_pipe( true )) {
	sysinfo.kernel.disconnect_pipe( pipe_no, true );
      }
      if( is_pipe( false )) {
	sysinfo.kernel.disconnect_pipe( pipe_no, false );
      }
      if( f != null ) {
	try { f.close( ); }
	catch ( IOException m ) {  ret = false; }
      }
      if( conn != null ) {
	try{ conn.close( ); }
	catch ( IOException m ) {  ret = false; }
      }
      if( sysinfo.verbose( )) {
	sysinfo.kernel.println( " Fileinfo.close( )   close done = " + ret);
      }
    }
    return( ret );
  }

  public String get_name( ) {
    String ret = null;
    if( isSOCKET( )) {
      ret = "<sock>";
      if(  conn != null ) { ret =  conn.toString( ); }
    }
    else {
      ret = name;
    }
    return( ret );
  }

  // $B%Q%$%W$r%;%C%H$9$k!#(B
  public void set_pipe( int _pipe_no ) {
    pipe_no = _pipe_no;
  }
  
  // $B%Q%$%WF~=PNO$+$I$&$+$rJV$9!#(B
  public boolean isPIPE( ) {
    return( is_pipe( true ) || is_pipe( false ));
  }

  // $B%=%1%C%H$+$I$&$+$rJV$9!#(B
  public boolean isSOCKET( ) {
    return( socket_flag );
  }

  // $B%9%H%j!<%`%=%1%C%H$+$I$&$+$rJV$9(B
  public boolean isSTREAM( ) {
    return( socket_flag && ( dgram == null ));
  }

  // $BF~NO$^$?$O=PNO%Q%$%W$+!)(B
  public boolean is_pipe( boolean input_flag ) {
    if( input_flag ) {
      return( pipe_in_flag );
    }
    return( pipe_out_flag );
  }

  // $B%Q%$%W$,@\B3$5$l$F$$$k$+!)(B
  public boolean is_pipe_connected( Sysinfo sysinfo, Process process ) {
    return( sysinfo.kernel.is_pipe_connected( pipe_no ));
  }

  // $B%Q%$%W$rJ#@=$9$k!#(B
  public void duplicate_pipe( Sysinfo sysinfo ) {
    if( is_pipe( true )) {
      sysinfo.kernel.duplicate_pipe( pipe_no, true );
    }
    else {
      sysinfo.kernel.duplicate_pipe( pipe_no, false );
    }
  }

  // IP$B%"%I%l%9$rJV$9(B
  public int get_ip_address( ) {
    int p = ip;
    if( conn != null ) {
      InetAddress addr = conn.getLocalAddress( );
      p = Util.swap32( Util.ip( addr.getHostAddress( )));
    }
    return( p );
  }

  // $B@\B3@h$N(BIP$B%"%I%l%9$rJV$9(B
  public int get_partner_ip_address( ) {
    InetAddress addr = conn.getInetAddress( );
    return( Util.swap32( Util.ip( addr.getHostAddress( ))));
  }

  // IP$B%"%I%l%9$r@_Dj$9$k(B
  public void set_ip_address( int _ip ) {
    ip = _ip;
    ip_str = Util.ip_str( Util.swap32( _ip ));
  }

  // $B%]!<%HHV9f$rJV$9(B
  public int get_port( ) {
    int p = port;
    if( conn  != null ) { p =  conn.getLocalPort( ); }
    if( sconn != null ) { p = sconn.getLocalPort( ); }
    return( p );
  }

  // $B@\B3@h$N%]!<%HHV9f$rJV$9(B
  public int get_partner_port( ) {
    return( conn.getPort( ));
  }

  // $B%]!<%HHV9f$r@_Dj$9$k(B
  public void set_port( int _port ) {
    port = _port;
  }

  // $B%G!<%?%@%$%"%0%i%`$rAw?.$9$k(B
  public boolean sendto( byte buf[] ) {
    boolean ret = true;
    DatagramPacket p;
    InetAddress iaddr;
    try{ iaddr = InetAddress.getByName( ip_str ); }
    catch( UnknownHostException m ) { return( false ); }
    p = new DatagramPacket( buf, buf.length, iaddr, port );
    try { dgram.send( p ); }
    catch( IOException m ) { ret = false; }
    return( ret );
  }

  // $B%G!<%?%@%$%"%0%i%`$r<u?.$9$k(B
  public int recvfrom( byte buf[], int addr_info[] ) {
    int ret = 0;
    int i;
    InetAddress iaddr;
    byte recv_buf[];
    DatagramPacket p = new DatagramPacket( buf, buf.length );

    // $B<u?.(B
    try { dgram.receive( p ); }
    catch( IOException m ) { return( -1 ); }

    // $BLa$jCM$N@_Dj(B
    recv_buf = p.getData( );
    ret      = p.getLength( );
    for( i = 0 ; i < ret ; i++ ) {
      buf[i] = recv_buf[i];
    }
    iaddr = p.getAddress( );
    addr_info[0] = Util.swap32( Util.ip( iaddr.getHostAddress( )));
    addr_info[1] = p.getPort( );

    //    System.out.println( " Fileinfo.recvfrom( )  iaddr.toString( ) = " + iaddr.getHostAddress( ));
    //    System.out.println( " Fileinfo.recvfrom( )  p.getPort( ) = " + p.getPort( ));

    return( ret );
  }

  // back_log$B?t$r@_Dj$9$k!#(B
  public void set_back_log( int _back_log ) {
    back_log = _back_log;
  }
}

