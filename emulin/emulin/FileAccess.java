// ----------------------------------------
//  Emulin FileAccess
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/02/10 18:59:40 $ 
//  $Id: FileAccess.java,v 1.48 2000/02/10 18:59:40 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import java.util.*;
import emulin.*;

public class FileAccess
{
  Process process;
  Sysinfo sysinfo;
  static int SEEK_SET = 0;
  static int SEEK_CUR = 1;
  static int SEEK_END = 2;
  Vector flist;

  FileAccess( ) {
    flist = new Vector( );
  }

  // $B;XDj%$%s%9%?%s%9$N>pJs$G<+J,$r%"%C%W%G!<%H$9$k!#(B
  public void update_info( FileAccess _p ) {
    sysinfo = _p.sysinfo;
  }

  // $B%Q%$%W$N@\B3=hM}(B
  public void pipe_connection( FileAccess _p ) {
    int i;
    int pipe_in_fd = -1;
    int pipe_out_fd = -1;
    Fileinfo finfo = null;
    // 1) $BA4$F$N%U%!%$%k%]%$%s%?$r%3%T!<$9$k(B
    for( i = 0 ; i < _p.flist.size( ) ; i++ ) {
      finfo = (Fileinfo)_p.flist.elementAt( i );
      if( sysinfo.verbose( )) {
	if( finfo == null ) {
	  process.println( " FileAccess.pipe_connection : fd = " + i + " finfo = " + finfo );
	}
	else {
	  process.println( " FileAccess.pipe_connection : fd = " + i + " finfo = " + finfo + " in = " + finfo.is_pipe( true ) + " out = " + finfo.is_pipe( false ) ); 
	}
      }
      if( finfo != null ) {
	// $B%Q%$%W$N>l9g$OB?=E2=$9$k!#(B
	if( finfo.isPIPE( )) {
	  finfo = finfo.duplicate( );
	  finfo.duplicate_pipe( sysinfo );
	}
      }
      flist.addElement( (Object)finfo );
    }
  }

  // $BA4$F$N%U%!%$%k$r%/%m!<%:$9$k!#(B
  public void all_file_close( ) {
    int i;
    for( i = 0 ; i < flist.size( ) ; i++ ) {
      Fileinfo finfo = (Fileinfo)flist.elementAt( i );
      if( sysinfo.verbose( )) {
	process.println( " all_file_close: i = " + i + " finfo = " + finfo );
      }
      if( null != finfo ) {
	FileClose( i );
	if( sysinfo.verbose( )) {
	  process.println( " all_file_close: i = " + i + " closed " );
	}
      }
    }
  }

  // $B%U%!%$%k%*!<%W%s;~$N%b!<%I%S%C%H$rJV$9(B
  public int GetModeBit( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    return( finfo.get_mode_bit( ));
  }

  // $B%U%!%$%k$r%*!<%W%s$9$k(B
  public int FileOpen( String vpath, String mode, int mode_bit ) {
    boolean open_flag = false;
    Fileinfo finfo = new Fileinfo( );
    String path = sysinfo.get_native_path( vpath );
    Inode inode = new Inode( vpath , sysinfo );
    // $B%G%#%l%/%H%j$J$i<B:]$K%*!<%W%s$O$;$:(B fd $B$rJV$9(B ( Linux $B%"%W%j$+$i8+$l$P(B open $B@.8y(B )
    if( inode.isDirectory( )) {
      finfo.opendir( path );
      open_flag = true;
    }
    else {
	if( sysinfo.kernel.is_device( vpath )) {
	    if( null != sysinfo.kernel.is_exist_device( vpath )) {
		path = sysinfo.kernel.is_exist_device( vpath );
		/* $B%G%P%$%9$r%*!<%W%s$9$k(B */
		if( finfo.open( path, mode, mode_bit )) { open_flag = true; }
	    }
	    else {
		return( -1 ); /* $B%*!<%W%s<:GT(B */
	    }
	}
	else {
	    // $B%U%!%$%k$J$i(Bjava$B$N5!G=$r;H$C$F%U%!%$%k%*!<%W%s$9$k(B
	    if( finfo.open( path, mode, mode_bit )) { open_flag = true; }
	}
    }
    if( open_flag ) {
      int _fd = search_empty_fd( );
      if( _fd == flist.size( )) { flist.addElement( (Object)finfo );        }
      else {                      flist.setElementAt( (Object)finfo, _fd ); }
      if( sysinfo.verbose( )) {
	int i;
	for( i = 0 ; i < flist.size( ) ; i++ ) {
	  if( i == _fd ) {
	    process.println( "  FileOpen : *fd = " + i + " flist = " + flist.elementAt( i ) );
	  }
	  else {
	    process.println( "  FileOpen :  fd = " + i + " flist = " + flist.elementAt( i ) );
	  }
	}
      }
      return( _fd );
    }
    return( -1 );
  }

  // $B6u$N(B fd $B$rJV$9(B ( $BHV9f$N<c$$$[$&$+$i(B )
  public int search_empty_fd( ) {
    int _fd = flist.size( );
    int i;
    // $B$b$7HV9f$N<c$$HV9f$,6u$$$F$$$l$P$=$l$r;H$&(B
    for( i = 0 ; i < flist.size( ) ; i++ ) {
      Fileinfo _finfo = (Fileinfo)flist.elementAt( i );
      if( sysinfo.debug( )) { process.println( "  FileOpen : fd = " + i + " flist = " + _finfo ); }
      if( _finfo == null )  { _fd = i; break; } // $B6u$$$F$$$?(B
    }
    return( _fd );
  }

  // $B%U%!%$%k$r%/%m!<%:$9$k(B
  boolean FileClose( int fd ) {
    boolean ret = true;
    if( fd >= flist.size( )) { return( true ); } // $B%*!<%W%s$7$F$$$J$$(B fd $BHV9f$O$D$M$K%/%m!<%:@.8y$H$9$k!#(B
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) {
      return( true );
    }
    ret = finfo.close( sysinfo );
    if( sysinfo.verbose( )) {
      process.println( "  FileClose : fd = " + fd );
    }
    flist.setElementAt( (Object)null, fd ); // $B%a%b%j$r3+J|$9$k!#$+$o$j$K(B null $B%*%V%8%'%/%H$r$V$i2<$2$F$*$/(B
    return( ret );
  }

  // $B%U%!%$%k$N%j!<%I$r9T$&(B
  synchronized int FileRead( int fd, byte buf[] ) {
    int ret = 0;
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) {  // $BL58z$J(B fd $B$J$i(B
      return( -1 );
    }
    if( sysinfo.verbose( )) {
      process.println( " FileAccess.FileRead( ) " );
    }
    if( finfo.is_pipe( true )) { // $B%Q%$%W(B
      ret = sysinfo.kernel.pipe_read( finfo.pipe_no, buf );
      if( sysinfo.verbose( )) {
	process.println( " FileRead (pipe) : pipe_no = " + finfo.pipe_no + " ret = " + ret );
      }
    }
    else { // $B$=$l0J30(B
      if( sysinfo.verbose( )) {
	process.println( "isOPEN( ) =   " + finfo.isOPEN( ));
	process.println( "Avaialbe( ) = " + finfo.Available( ));
      }

      if( sysinfo.verbose( )) {
	  process.println( " FileRead (file) : start" );
      }

      if( null != finfo.subprocess ) {
	  if( sysinfo.verbose( )) {
	      process.println( " FileRead (file) : ( use subprocess )" );
	  }
	  ret = finfo.subprocess.read_byte_top( buf, false );
      }
      else {
	  if( sysinfo.verbose( )) {
	      process.println( " FileRead (file) : ( use finfo.Read( )  )" );
	  }
	  ret = finfo.Read( buf );
      }
      if( sysinfo.verbose( )) {
	  process.println( " FileRead (file) : end" );
      }
    }
    return( ret );
  }

  // $B%U%!%$%k$N=q$-9~$_$r9T$&(B
  boolean FileWrite( int fd, byte buf[] ) {
    boolean ret = true;
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) {
      process.println( "FileWrite( )  finfo is NULL   fd = " + fd );
      return( false );
    }

    if( sysinfo.verbose( )) {
      process.println( " FileAccess.FileWrite( ) " );
    }
    if( finfo.is_pipe( false )) { // $B%Q%$%W(B
      ret = sysinfo.kernel.pipe_write( finfo.pipe_no, buf );
      if( sysinfo.verbose( )) {
	process.println( " FileWrite (pipe) : pipe_no = " + finfo.pipe_no  + " ret = " + ret );
      }
    }
    else { // $B$=$l0J30(B
      if( sysinfo.verbose( )) {
	process.println( " FileWrite (file or socket) : " );
      }
      ret =  finfo.Write( buf );
    }
    return( ret );
  }

  // $B%U%!%$%k%7!<%/(B
  int FileSeek( int fd, int offset, int whence ) {
    boolean ret = true;
    int o = 0;
    int size = 0;
    int curptr = 0;
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) {
      process.println( "FileSeek( ) : finfo is NULL   fd = " + fd );
    }

    if( sysinfo.debug( )) {
      process.println( "  FileSeek : fd = " + fd + " offset = " + offset );
      int i;
      for( i = 0 ; i < flist.size( ) ; i++ ) {
	process.println( "  FileSeek : fd = " + i + " flist = " + flist.elementAt( i ) );
      }
    }
    if( null != finfo.f ) { // $B%G%#%l%/%H%j0J30$J$i<B:]$N%7!<%/$r<B9T$9$k(B
      try { curptr = (int)finfo.f.getFilePointer( ); }
      catch ( IOException m ) { ret = false; }
      try { size = (int)finfo.f.length( ); }
      catch ( IOException m ) { ret = false; }
      if( ret ) {
	if( whence == SEEK_SET ) {
	  o = offset;
	}
	if( whence == SEEK_CUR ) {
	  o = curptr + offset;
	}
	if( whence == SEEK_END ) {
	  o = size + offset;
	}
	try { finfo.f.seek( o ); }
	catch ( IOException m ) { ret = false; }
      }
    }
    else { // $B%G%#%l%/%H%j$N>l9g$O%]%$%s%?$N$_JQ99$7$F$*$/(B
      curptr = get_ptr( fd );
      if( whence == SEEK_SET ) {
	o = offset;
      }
      if( whence == SEEK_CUR ) {
	o = curptr + offset;
      }
      if( whence == SEEK_END ) {
	o = size + offset;
      }
      set_ptr( fd, o );
    }
    if( ret ) {
      return( o );
    }
    return( 0 );
  }

  // $B%U%!%$%kL>$rJV$9(B
  String get_name( int fd ) {
    String ret = null;
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( "<noname>" ); }
    if( finfo.isSOCKET( )) {
      ret = finfo.get_name( );
    }
    else {
      ret = sysinfo.get_virtual_path( finfo.get_name( ));
    }
    return( ret );
  }

  // $B%U%!%$%k$N%+%l%s%H0LCV$r%;%C%H$9$k(B
  void set_ptr( int fd, int ptr ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    finfo.set_ptr( ptr );
  }
  // $B%U%!%$%k$N%+%l%s%H0LCV$rFI$_=P$9(B
  int get_ptr( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    return( finfo.get_ptr( ));
  }

  // $B;XDj%U%!%$%k$N>pJs%$%s%9%?%s%9$rJV$9(B
  Fileinfo get_finfo( int fd ) {
    return( (Fileinfo)flist.elementAt( fd ));
  }

  // $BI8=`F~=PNO$+$I$&$+$rJV$9(B
  boolean isSTD( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( false ); }
    return( finfo.isSTD( ));
  }

  // $B%(%i!<F~=PNO$+$I$&$+$rJV$9(B
  boolean isERR( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( false ); }
    return( finfo.isERR( ));
  }

  // $B%Q%$%WF~=PNO$+$I$&$+$rJV$9!#(B
  boolean isPIPE( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( false ); }
    return( finfo.is_pipe( true ) || finfo.is_pipe( false ));
  }

  // $B%=%1%C%H$+$I$&$+$rJV$9!#(B
  boolean isSOCKET( int fd ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( false ); }
    return( finfo.isSOCKET( ));
  }

  // $B%Q%$%WF~NO$^$?$O=PNO$+$I$&$+$rJV$9!#(B
  boolean is_pipe( int fd, boolean input_flag ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( fd );
    if( finfo == null ) { return( false ); }
    return( finfo.is_pipe( input_flag ) );
  }

  // $B;XDj%U%!%$%k$r>C5n$9$k!#(B
  public boolean unlink( String vpath ) {
    File file;
    file = new File( sysinfo.get_native_path( vpath ));
    return( file.delete( ));
  }

  // $B;XDj%U%!%$%k$NL>A0$rJQ99$9$k(B
  public boolean rename( String vpath_from, String vpath_to ) {
    File file_from, file_to;
    String path_from = sysinfo.get_native_path( vpath_from );
    String path_to   = sysinfo.get_native_path( vpath_to   );
    file_from = new File( path_from );
    file_to   = new File( path_to );
    return( file_from.renameTo( file_to ));
  }

  // $B;XDj%G%#%l%/%H%j$N:n@.$r9T$&(B
  public boolean mkdir( String vpath ) {
    String path = sysinfo.get_native_path( vpath );
    File file;
    file = new File( path );
    return( file.mkdir( ));
  }

  // fd$BHV9f$,(B from $BHV$N%U%!%$%k$r(B to $BHV$KJ#@=$9$k!#(B
  public void Dup( int from, int to ) {
    Fileinfo finfo = (Fileinfo)flist.elementAt( from );
    if( sysinfo.verbose( )) {
      process.println( " Dup   finfo = " + finfo );
    }
    while( to >= flist.size( )) {
      flist.addElement( (Object)null );
    }
    flist.setElementAt( (Object)finfo, to );
    if( sysinfo.verbose( )) {
      process.println( " Dup ( " + from + "," + to + " );  isSTD( to ) = " + finfo.isSTD( ));
    }
    if( finfo.isPIPE( )) { finfo.duplicate_pipe( sysinfo ); }
    else {                 finfo.duplicate_file( sysinfo ); }
  }

  // $B;XDj%G%#%l%/%H%jFb$N%(%s%H%j%j%9%H$rJV$9(B
  // ( Java API $B$GF@$?%j%9%H$K(B . .. $B$rDI2C$9$k!#(B )
  public String [] file_list( String vpath ) {
    int i;
    String _list[];
    String list[];
    int slide;
    File file = new File( sysinfo.get_native_path( vpath ));

    _list = file.list( );

    if( sysinfo.verbose( )) {
      process.println( "FileAccess.file_list( )  file = " + file + " _list.length = " + _list.length );
    }

    if( 0 == _list.length ) {  // $B%j%9%H$,6u$J$i(B,$BG[NsMWAG$rJd$&(B
      _list = new String[ 1 ];
      _list[0] = ".";
    }
    list = _list;

    slide = 1;
    // . .. $B$rDI2C$9$k(B
    if( !_list[0].equals( "." )) {
      slide = 3;
    }
    list = new String[ _list.length + slide ];
    list[0] = ".";
    if( slide == 3 ) {
      list[1] = ".";
      list[2] = "..";
    }
    for( i = 0 ; i < _list.length ; i++ ) {
      list[i + slide] = _list[i];
    }
    return( list );
  }

  // $B%Q%$%W$,$"$k$+$I$&$+$rJV$9!#(B
  public int search_pipe( boolean input_flag ) {
    int i;
    Fileinfo finfo;
    // $B=PNO%Q%$%W$rC5$9(B
    for( i = 0 ; i < flist.size( ) ; i++ ) {
      finfo = (Fileinfo)flist.elementAt( i );
      if( null != finfo ) {
	if( finfo.is_pipe( input_flag ))  { return( i ); }
      }
    }
    return( -1 ); // $B8+IU$+$i$J$+$C$?!#(B
  }

  // $B=PNO%Q%$%W$,@\B3$5$l$F$$$k$+$rJV$9!#(B
  public boolean is_pipe_connected( int fd ) {
    int i;
    Fileinfo finfo;
    // $B=PNO%Q%$%W$rC5$9(B
    finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) {
      return( false );
    }
    return( finfo.is_pipe_connected( sysinfo, process ));
  }

  // $B=PNO%Q%$%W$KF~NO$r@\B3$9$k!#(B
  public boolean set_pipe( int pipe_no, int fd ) {
    Fileinfo finfo;
    finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) { return( false ); }
    finfo.set_pipe( pipe_no );
    if( sysinfo.verbose( )) {
      process.println( "  set_pipe( ) : pipe_no = " + pipe_no + " fd = " + fd );
    }
    return( true );
  }

  // $B$J$s$i$+$N%$%Y%s%H$,$"$C$?$+!)(B
  public boolean isEvent( int fd ) {
    boolean ret = false;
    Fileinfo finfo;
    finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) { return( false ); }
    return( finfo.isEvent( ));
  }

  // $B$@$l$+$N<j$K$h$C$F4{$K%/%m!<%:$5$l$?$+!)(B
  public boolean isClosed( int fd ) {
    boolean ret = false;
    Fileinfo finfo;
    finfo = (Fileinfo)flist.elementAt( fd );
    if( null == finfo ) { return( false ); }
    if( finfo.isPIPE( )) {
      if( !finfo.is_pipe_connected( sysinfo, process )) {
	ret = true;
      }
    }
    if( finfo.isSOCKET( )) {
      if( !finfo.is_connected( )) {
	ret = true;
      }
    }
    if( sysinfo.verbose( )) {
      process.println( " isClosed( " + fd + " ) = " + ret );
    }
    return( ret );
  }
}
