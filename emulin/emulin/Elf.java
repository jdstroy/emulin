// ----------------------------------------
//  Elf ( support ELF format (32bit for i[34]86))
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/11/30 17:23:08 $ 
//  $Id: Elf.java,v 1.24 1999/11/30 17:23:08 kiyoka Exp $
// ----------------------------------------
package emulin;

//
// ELF$B%X%C%@(B
//
// typedef struct elf32_hdr{
//   EI_NIDENT = 16;
//   char	e_ident[EI_NIDENT];      :  e_ident[0...3] = '\x7f' "ELF" $B$G$"$k$3$H(B
//   Elf32_Half	e_type;                  :  ET_EXEC $B$G$"$k$3$H(B
//   Elf32_Half	e_machine;               :  EM_386 $B$G$"$k$3$H(B ($B>-Mh(B EM_486$B%5%]!<%H$9$k(B)
//   Elf32_Word	e_version;
//   Elf32_Addr	e_entry;  /* Entry point */
//   Elf32_Off	e_phoff;
//   Elf32_Off	e_shoff;
//   Elf32_Word	e_flags;
//   Elf32_Half	e_ehsize;
//   Elf32_Half	e_phentsize;
//   Elf32_Half	e_phnum;
//   Elf32_Half	e_shentsize;
//   Elf32_Half	e_shnum;
//   Elf32_Half	e_shstrndx;
// } Elf32_Ehdr;

// Emulin $B$N%;%0%a%s%H$N07$$(B
//  1. stack
//     $B%"%I%l%9(B ~0 $B$+$i(B $BE,Ev$J%a%b%j$r3NJ]$9$k!#(B
//     ELF$B$N$J$+$K(B stack$B%;%0%a%s%H$OL5$$$N$G(B,Elf$B%/%i%9$GDI2C@8@.$9$k!#(B
//  2. $B$=$l0J30$N%;%0%a%s%H(B
//     $B%;%0%a%s%H$N(BWRX$BB0@-$K$7$?$,$C$F(B,$B%"%/%;%9@)8B$r9T$&!#(B
//     $BEvA3(B entry $B%]%$%s%H$O(B X$BB0@-$N%;%0%a%s%H$G$J$1$l$P%(%i!<$H$9$k!#(B

import java.lang.*;
import java.io.*;
import emulin.*;

public class Elf
{
  Process process;

  static short ET_NONE = 0;		/* No file type */
  static short ET_REL  = 1;		/* Relocatable file */
  static short ET_EXEC = 2;		/* Executable file */
  static short ET_DYN  = 3;		/* Shared object file */
  static short ET_CORE = 4;		/* Core file */
  static short ET_NUM  = 5;		/* Number of defined types.  */

  /* These constants define the various ELF target machines */
  static short EM_386  = 3;
  static short EM_486  = 6;   /* Perhaps disused */

  byte  e_ident[] = new byte[16];           //   :  e_ident[0...3] = '\x7f' "ELF" $B$G$"$k$3$H(B
  short	e_type;                       
  short	e_machine;
  int e_version;
  int e_entry;              // Entry point
  int e_phoff;
  int e_shoff;
  int e_flags;
  short	e_ehsize;
  short	e_phentsize;
  short	e_phnum;
  short e_shentsize;
  short e_shnum;
  short e_shstrndx;
  String symbol[];
  int symadrs[];
  int symbols;
  Segment[] segment;       // $B%;%0%a%s%H(B
  int segments;            // $BAm%;%0%a%s%H?t(B
  Section[] section;       // $B%;%/%7%g%s(B
  int sections;            // $BAm%;%/%7%g%s?t(B
  int brk;                 // $B8=:_$N(B brk $B%"%I%l%9(B
  int brk_segment_no;      // brk$B$NB8:_$9$k(B $B%;%0%a%s%HHV9f(B
  Sysinfo sysinfo;


  // $B;XDj%$%s%9%?%s%9$N>pJs$G<+J,$r%"%C%W%G!<%H$9$k!#(B
  public void update_info( Elf _elf ) {
    int i;
    System.arraycopy( _elf.e_ident, 0, e_ident, 0, _elf.e_ident.length );
    e_type       = _elf.e_type       ;
    e_machine    = _elf.e_machine    ;
    e_version    = _elf.e_version    ;
    e_entry      = _elf.e_entry      ;
    e_phoff      = _elf.e_phoff      ;
    e_shoff      = _elf.e_shoff      ;
    e_flags      = _elf.e_flags      ;
    e_ehsize     = _elf.e_ehsize     ;
    e_phentsize  = _elf.e_phentsize  ;
    e_phnum      = _elf.e_phnum      ;
    e_shentsize  = _elf.e_shentsize  ;
    e_shnum      = _elf.e_shnum      ;
    e_shstrndx   = _elf.e_shstrndx   ;
    // $B%7%s%\%k$N%3%T!<(B
    symbols      = _elf.symbols      ;
    symbol       = new String[ _elf.symbols ];
    symadrs      = new int[ _elf.symbols ];
    if( sysinfo.debug( )) {
      process.println( "  Elf.update_info : symbols = [ " + symbols + " ] " );
    }
    for( i = 0 ; i < symbols ; i++ ) {
      if( sysinfo.debug( )) {
	process.println( "  Elf.update_info : symbol[ " + i + " ]  = " + _elf.symbol[i] + " " );
      }
      if( null != _elf.symbol[i] ) {
	symbol[i]  = new String( _elf.symbol[i] );
	symadrs[i] = _elf.symadrs[i];
      }
    }
    if( sysinfo.verbose( )) {
      process.println( "  Elf.update_info : Symbol copyied 1" );
    }
    // $B%;%0%a%s%H(B
    segments = _elf.segments;
    segment  = new Segment[ segments ];
    for( i = 0 ; i < segments ; i++ ) {
      segment[i] = _elf.segment[i].duplicate( );
    }
    // $B%;%/%7%g%s(B
    sections = _elf.sections;
    section  = new Section[ sections ];
    for( i = 0 ; i < segments ; i++ ) {
      section[i] = _elf.section[i].duplicate( );
    }
    brk            = _elf.brk;               // $B8=:_$N(B brk $B%"%I%l%9(B
    brk_segment_no = _elf.brk_segment_no;    // brk$B$NB8:_$9$k(B $B%;%0%a%s%HHV9f(B
  }

  public boolean load_symbol( String filename ) {
    String buf = "";
    String adrs_str = "";
    int i;
    RandomAccessFile in;
    try { in = new RandomAccessFile( sysinfo.get_native_path( filename ), "r" ); }
    catch ( IOException m ) {
      if( sysinfo.debug( )) {
	process.println( "Can't file open :" + filename );
      }
      symbols = 0;
      return( false );   
    }
    // $B9T?t$N3NG'(B
    for( i = 0 ; null != buf ; i++ ) {
      try { buf = in.readLine( ); }
      catch ( IOException m ) {  process.println( "File read error" ); return( false ); }
    }
    symbol  = new String[i];
    symadrs = new int[i];
    // $BFI$_9~$_(B
    try{ in.seek( 0 ); }
    catch ( IOException m ) {  process.println( "Seek Failed :" + filename ); return( false ); }    

    buf = "";
    for( i = 0 ; null != buf ; i++ ) {
      try { buf = in.readLine( ); }
      catch ( IOException m ) {  process.println( "File read error" ); return( false ); }
      if( buf != null ) {
	adrs_str = buf.substring( 0, 8 );
	if( ! adrs_str.equals( "        " )) {
	  symadrs[ i ] = Integer.parseInt( adrs_str, 16);
	  symbol[ i ] = buf.substring( 11 );
	  if( sysinfo.debug( )) {
	    //	    process.println( "adrs = " + Util.hexstr( symadrs[i], 8 ) + "  symbol = " + symbol[i] );
	  }
	}
      }
    }
    symbols = i;
    return( true );
  }

  public String get_symbol( int address ) {
    int i;
    String ret = null;
    for( i = 0 ; i < symbols ; i++ ) {
      if( address == symadrs[i] ) {
	ret = symbol[i];
      }
    }
    return( ret );
  }

  // $B%(%s%H%j!<%"%I%l%9$rJV$9(B
  public int get_entry( ) {
    return( e_entry );
  }

  // brk$BCM(B($B%G!<%?%;%0%a%s%H$N:G8e$N%"%I%l%9(B)$B$rJV$9(B
  public int get_curbrk( ) {
    return( brk );
  }

  // brk$BCM$r99?7$9$k(B
  public boolean set_curbrk( int _brk ) {
    if( segment[ brk_segment_no ].expand_memory( _brk )) {
      brk = _brk;
    }
    return( true );
  }

  // $B%m!<%I$9$k(B
  public boolean load( String filename ) {
    RandomAccessFile in;
    int i;
    try { in = new RandomAccessFile( sysinfo.get_native_path( filename ), "r" ); }
    catch ( IOException m ) {  process.println( "Can't file open :" + filename );  return( false ); }

    // $B%X%C%@>pJs$rA4$F%m!<%I$9$k(B
    LoadUtil.bytes( in, e_ident, sysinfo.kernel );
    e_type        =   LoadUtil.little16( in, sysinfo.kernel );
    e_machine     =   LoadUtil.little16( in, sysinfo.kernel );
    e_version     =   LoadUtil.little32( in, sysinfo.kernel );
    e_entry       =   LoadUtil.little32( in, sysinfo.kernel );
    e_phoff       =   LoadUtil.little32( in, sysinfo.kernel );
    e_shoff       =   LoadUtil.little32( in, sysinfo.kernel );
    e_flags       =   LoadUtil.little32( in, sysinfo.kernel );
    e_ehsize      =   LoadUtil.little16( in, sysinfo.kernel );
    e_phentsize   =   LoadUtil.little16( in, sysinfo.kernel );
    e_phnum       =   LoadUtil.little16( in, sysinfo.kernel );
    e_shentsize   =   LoadUtil.little16( in, sysinfo.kernel );
    e_shnum       =   LoadUtil.little16( in, sysinfo.kernel );
    e_shstrndx    =   LoadUtil.little16( in, sysinfo.kernel );

    if( sysinfo.debug( )) {
      process.println( "File [" + filename + "]" );
      process.println( "----- Elf Header -----" );
      process.println( "e_type        : " + Integer.toString( e_type,         16));
      process.println( "e_machine     : " + Integer.toString( e_machine,      16));
      process.println( "e_version     : " + Integer.toString( e_version,      16));
      process.println( "e_entry       : " + Integer.toString( e_entry,        16));
      process.println( "e_phentsize   : " + Integer.toString( e_phentsize,    16));
      process.println( "e_phnum       : " + Integer.toString( e_phnum,        16));
      process.println( "e_phoff       : " + Integer.toString( e_phoff,        16));
      process.println( "e_shnum       : " + Integer.toString( e_shnum,        16));
      process.println( "e_shoff       : " + Integer.toString( e_shoff,        16));
    }

    // ELF$B%U%)!<%^%C%H$+$I$&$+3NG'$9$k(B
    if( !
       ((e_ident[0] == 0x7F ) &&
       (e_ident[1] == 'E' ) &&
       (e_ident[2] == 'L' ) &&
       (e_ident[3] == 'F' ))) {
      process.println( "Not Elf Format :" + filename ); return( false );
    }

    if( (e_type != ET_EXEC) ) {
      process.println( "Not Executable Format :" + filename ); return( false );
    }
    if( e_machine != EM_386 ) {
      process.println( "Not Match CPU Type :" + filename ); return( false );
    }

    // $B%W%m%0%i%`%X%C%@$rFI$_9~$`(B
    try{ in.seek( e_phoff ); }
    catch ( IOException m ) {  process.println( "Seek Failed :" + filename ); return( false ); }    

    segments = e_phnum + 1;
    segment = new Segment[ segments ];
    for( i = 0 ; i < segments ; i++ ) {
      segment[i] = new Segment( sysinfo, process );
      if( i < e_phnum ) {
	segment[i].load_ph( in );
      }
      else {
	segment[i].stack( Sysinfo.stack_size );
      }
    }
    // $B%\%G%#!<$N%m!<%I(B
    for( i = 0 ; i < e_phnum ; i++ ) {
      segment[i].load_body( in );
    }

    // $B%;%/%7%g%s%X%C%@$rFI$_9~$`(B
    try{ in.seek( e_shoff ); }
    catch ( IOException m ) {  process.println( "Seek Failed :" + filename ); return( false ); }    
    sections = e_shnum;
    section = new Section[ sections ];
    for( i = 0 ; i < sections ; i++ ) {
      section[i] = new Section( sysinfo, process );
      section[i].load( in );
      if( section[i].isbss( )) {
	brk = section[i].get_brk( );
      }
    }

    // brk$B%"%I%l%9$,4^$^$l$k%;%0%a%s%H$rC5$9(B
    for( i = 0 ; i < segments ; i++ ) {
      if( brk == segment[i].segment_end( )) {
	brk_segment_no = i;
      }
    }

    if( sysinfo.debug( )) {
      process.println( " ----- BRK ----- " );
      process.println( "   brk adrs       = " + Util.hexstr( brk, 8 ));
      process.println( "   brk segment no = " + Util.hexstr( brk_segment_no, 8 ));
    }
    return( true );
  }
}
