// ----------------------------------------
//  IA-32 Cpu Decoder (no support 16bit code)
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/09/15 11:36:24 $ 
//  $Id: XDecoder.java,v 1.47 1999/09/15 11:36:24 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import emulin.*;

public class XDecoder
{
  Process process;
  int inst_num;
  Instruction inst[];

  static byte OperandSizePrefix = 0x66;
  static byte AddressSizePrefix = 0x67;
  static byte REPNZ = (byte)0xF2;
  static byte REPZ  = (byte)0xF3;
  static String cond_str[] = {
    "O",    "NO",    "B",    "AE",    "E",    "NE",    "BE",    "A",    "S",    "NS",    "P",
    "NP",   "L",     "GE",   "LE",    "G"
  };
  static int JO  = 0;
  static int JNO = 1;
  static int JB  = 2;
  static int JAE = 3;
  static int JE  = 4;
  static int JNE = 5;
  static int JBE = 6;
  static int JA  = 7;
  static int JS  = 8;
  static int JNS = 9;
  static int JP  = 10;
  static int JNP = 11;
  static int JL  = 12;
  static int JGE = 13;
  static int JLE = 14;
  static int JG  = 15;

  short inst_search_list[];
  Sysinfo sysinfo;         // $B%7%9%F%`>pJs(B ( Cpu$B%/%i%9$N@8@.;~$K=i4|2=$5$l$k(B )

  Decodeinfo dinfo;        // $B%G%3!<%I7k2L>pJs(B
  Decodeinfo dcache[];     // $B%G%3!<%I%-%c%C%7%e(B
  short cached_address[];  // $B%-%c%C%7%e$5$l$?%"%I%l%9(B ( $B%"%I%l%9$N>e0L(B16bit )
  int hits;                // $B%-%c%C%7%e$K%R%C%H$7$?2s?t(B
  int trys;                // $B%-%c%C%7%e$,;H$($k$+%A%'%C%/$7$?2s?t(B

  public XDecoder( ) {
    int i;
    inst_num = 256;
    inst = new Instruction[inst_num];

    for( i = 0 ; i < inst_num ; i++ ) {
      inst[i] = new Instruction( );
    }
    dinfo = new Decodeinfo( );
    dinfo.src = new Operand( );
    dinfo.dst = new Operand( );
    dinfo.fst = new Operand( );

    //    dcache         = new Decodeinfo[0x10000];
    //    cached_address = new short[0x10000];
    hits = 0;
    trys = 0;
  }

  // $BL?Na%G!<%?$NDI2C(B
  void _add_inst( int opecode_index, int inst_id, String name, int opecodes,
		  byte d0, byte w0, byte W0, byte s0, byte r0, byte c0, byte D0,
		  byte d1, byte w1, byte W1, byte s1, byte r1, byte c1, byte D1,
		  byte opecode0, byte opecode1, byte mask0, byte mask1,  char operand_key ) {
    inst[opecode_index].set_info( name, inst_id, opecodes, 
				d0, w0, W0, s0, r0, c0, D0,
				d1, w1, W1, s1, r1, c1, D1,
				opecode0, opecode1, mask0, mask1, operand_key );
  }

  // $B%G%3!<%I$7$?L?Na%3!<%I$rJV$9(B
  public int get_inst_id( ) {
    return( dinfo.inst_id );
  }

  // $B%-%c%C%7%e$K%R%C%H$9$k$+%A%'%C%/$9$k!#(B
  public boolean cache_check( int ip ) {
    // $B%-%c%C%7%e$K%R%C%H$9$k$+%A%'%C%/$9$k!#(B
    //    short high = (short)((ip >> 16) & 0xFFFF);
    //    int low = ip & 0xFFFF;
    boolean ret = false;
    //    if( sysinfo.cache( )) {
    //      if( cached_address[low] == high ) {
    //	//      hits++;
    //	ret = true;
    //      }
    //    }
    //    trys++;
    
    //    if( 0 == ( trys % 10000 )) {
    //      process.println( " hits/trys = (" + hits + "/" + trys + ") : " + ((double)hits*100.0) / (double)trys + "%" );
    //    }
    return( ret );
  }

  // $B%-%c%C%7%e$rGK4~$9$k(B
  public void cache_expire( ) {
    int i;
    //    for( i = 0 ; i < 0x10000 ; i++ ) {
    //      dcache[i] = null;
    //    }
    //    dcache = null;
    //    cached_address = null;
  }

  // $B%G%3!<%I$r9T$&(B
  public int decode( int ip, byte buf[], boolean use_cache ) {
    int len = 0;
    //    short high = (short)((ip >> 16) & 0xFFFF);
    int low = ip & 0xFFFF;

    if( use_cache ) {
      dinfo = dcache[low].duplicate( );
    }
    else {
      dinfo.d_flag = false;               // dist flag
      dinfo.w_flag = false;               // width flag
      dinfo.W_flag = false;               // width flag ( B/W$B%5%U%#%C%/%9IU$-(B )
      dinfo.s_flag = false;               // $BId9f3HD%%U%i%0(B
      dinfo.r_flag = false;               // $B%l%8%9%?%U%i%0(B
      dinfo.c_flag = false;               // $B>r7o(B
      dinfo.D_flag = false;               // $B>r7o(B

      dinfo.src.init( );                  // src$B%*%Z%i%s%I=i4|2=(B
      dinfo.dst.init( );                  // dst$B%*%Z%i%s%I=i4|2=(B
      dinfo.fst.init( );
      
      // $BL?Na$N7hDj(B
      dinfo.inst_index = _decide_inst_index( buf );
      // $B%*%Z%i%s%I$N2r@O(B
      if( dinfo.o16_flag ) { len++; }
      if( dinfo.repnz_flag ) { len++; }
      if( dinfo.repz_flag ) { len++; }
      // $B%*%Z%3!<%ICf$N%U%i%0$N2r@O(B
      inst_flag_analyze( buf, inst[dinfo.inst_index].opebytes, len );
      len = operand( buf, inst[dinfo.inst_index].opebytes+len );
      
      dinfo.inst_id = inst[dinfo.inst_index].id;
      dinfo.inst_len = len;

      //      if( sysinfo.cache( )) {
      //	if( dinfo.inst_len >= 2 ) {
      //	  dcache[low]         = dinfo.duplicate( );
      //	  cached_address[low] = high;
      //	}
      //      }
    }
    return( dinfo.inst_len );
  }

  // $BL?NaCf$N%U%i%0$N2r@O(B
  void inst_flag_analyze( byte buf[], int opebytes, int len ) {
    int i;
    for( i = 0 ; i < opebytes ; i++ ) {
      if( 0 != inst[dinfo.inst_index].r[i] ) {
	dinfo.r_val = (inst[dinfo.inst_index].r[i] & buf[i+len]);
	dinfo.r_flag = true;
	dinfo.r_index = i;
      }
      if( 0 != inst[dinfo.inst_index].w[i] ) {
	dinfo.w_val = (inst[dinfo.inst_index].w[i] & buf[i+len]);
	dinfo.w_flag = true;
	dinfo.w_index = i;
      }
      if( 0 != inst[dinfo.inst_index].W[i] ) {
	dinfo.W_val = (inst[dinfo.inst_index].W[i] & buf[i+len]);
	dinfo.W_flag = true;
	dinfo.W_index = i;
      }
      if( 0 != inst[dinfo.inst_index].c[i] ) {
	dinfo.c_val = (inst[dinfo.inst_index].c[i] & buf[i+len]);
	dinfo.c_flag = true;
	dinfo.c_index = i;
      }
      if( 0 != inst[dinfo.inst_index].d[i] ) {
	dinfo.d_val = (inst[dinfo.inst_index].d[i] & buf[i+len]);
	dinfo.d_flag = true;
	dinfo.d_index = i;
      }
      if( 0 != inst[dinfo.inst_index].s[i] ) {
	dinfo.s_val = (inst[dinfo.inst_index].s[i] & buf[i+len]);
	dinfo.s_flag = true;
	dinfo.s_index = i;
      }
      if( 0 != inst[dinfo.inst_index].D[i] ) {
	dinfo.D_val = (inst[dinfo.inst_index].D[i] & buf[i+len]);
	dinfo.D_flag = true;
	dinfo.D_index = i;
      }
    }

    // $B%=!<%9%*%Z%i%s%I;CDj(B
    if( dinfo.r_flag ) {
      dinfo.src.kind = Operand.REG;
      dinfo.src.reg_no = dinfo.r_val;
    }
  }


  // $B%*%Z%i%s%I$N2r@O$r9T$&(B
  int operand( byte buf[], int len ) {
    Operand temp;
    if( inst[dinfo.inst_index].operand_key == '-' ) {
      len = _operand_slash( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'W' ) {
      len = _operand_W( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'M' ) {
      len = _operand_M( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 't' ) {
      len = _operand_t( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'T' ) {
      len = _operand_T( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'm' ) {
      len = _operand_m( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'p' ) {
      len = _operand_p( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 's' ) {
      len = _operand_s( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'd' ) {
      len = _operand_d( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'V' ) {
      len = _operand_V( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'D' ) {
      len = _operand_D( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'e' ) {
      len = _operand_e( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'E' ) {
      len = _operand_E( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'n' ) {
      len = _operand_n( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'N' ) {
      len = _operand_N( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'o' ) {
      len = _operand_o( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'I' ) {
      len = _operand_I( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'i' ) {
      len = _operand_i( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'J' ) {
      len = _operand_J( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == '1' ) {
      len = _operand_1( buf, len );
    }
    if( inst[dinfo.inst_index].operand_key == 'C' ) {
      len = _operand_C( buf, len );
    }

    // src,dst $B$r5U$K$9$k(B
    if( dinfo.d_flag && ( dinfo.d_val != 0 )) {
      temp = dinfo.src;
      dinfo.src = dinfo.dst;
      dinfo.dst = temp;
    }
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B -$B$N2r@O$r9T$&(B
  int _operand_slash( byte buf[], int len ) {
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B M$B$N2r@O$r9T$&(B
  int _operand_M( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    return( len );
  }

  int _operand_W( byte buf[], int len ) {
    Operand temp;
    len = _operand_M( buf, len );
    temp = dinfo.src;
    dinfo.src = dinfo.dst;
    dinfo.dst = temp;
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B t$B$N2r@O$r9T$&(B
  int _operand_t( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.fst.kind = Operand.IMM;
    dinfo.fst.imm = buf[len];
    return( len+1 );
  }

  // $B%*%Z%i%s%I5-9f(B T$B$N2r@O$r9T$&(B
  int _operand_T( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.fst.kind = Operand.REG;
    dinfo.fst.reg_no = Cpu.CX;
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B m$B$N2r@O$r9T$&(B
  int _operand_m( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src = dinfo.dst;
    dinfo.dst = new Operand( );
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B p$B$N2r@O$r9T$&(B
  int _operand_p( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src = dinfo.dst;
    dinfo.dst = new Operand( );
    dinfo.dst.kind = Operand.REG;
    dinfo.dst.reg_no = Cpu.AX;
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B s$B$N2r@O$r9T$&(B
  int _operand_s( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src = dinfo.dst;
    dinfo.dst = new Operand( );
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B d$B$N2r@O$r9T$&(B
  int _operand_d( byte buf[], int len ) {
    dinfo.src.kind = Operand.DISP;
    dinfo.src.disp = (int)buf[len];
    return( len+1 );
  }

  // $B%*%Z%i%s%I5-9f(B V$B$N2r@O$r9T$&(B
  int _operand_V( byte buf[], int len ) {
    dinfo.src.kind = Operand.IMM;
    dinfo.src.imm = ((int)buf[len] & 0xFF);
    return( len+1 );
  }

  // $B%*%Z%i%s%I5-9f(B D$B$N2r@O$r9T$&(B
  int _operand_D( byte buf[], int len ) {
    dinfo.src.kind = Operand.DISP;
    dinfo.src.disp = Util.to32( buf, len );
    return( len+4 );
  }

  // $B%*%Z%i%s%I5-9f(B e$B$N2r@O$r9T$&(B
  int _operand_e( byte buf[], int len ) {
    dinfo.dst.kind    = Operand.REG;
    dinfo.dst.reg_no  = Cpu.AX;
    dinfo.src.kind    = Operand.MEM;
    dinfo.src.adrs    = Util.to32( buf, len );
    return( len+4 );
  }

  // $B%*%Z%i%s%I5-9f(B E$B$N2r@O$r9T$&(B
  int _operand_E( byte buf[], int len ) {
    dinfo.src.kind    = Operand.REG;
    dinfo.src.reg_no  = Cpu.AX;
    dinfo.dst.kind    = Operand.MEM;
    dinfo.dst.adrs    = Util.to32( buf, len );
    return( len+4 );
  }

  // $B%*%Z%i%s%I5-9f(B n$B$N2r@O$r9T$&(B
  int _operand_n( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src = dinfo.dst;
    if( Operand.EA == dinfo.src.kind ) {
      dinfo.src.kind = Operand.REA;
    }
    if( Operand.REG == dinfo.src.kind ) {
      dinfo.src.kind = Operand.IREG;
    }
    dinfo.dst = new Operand( );
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B N$B$N2r@O$r9T$&(B
  int _operand_N( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    len = _imm_analyze( buf, len );
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B o$B$N2r@O$r9T$&(B
  int _operand_o( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src.kind = Operand.IMM;
    dinfo.src.imm = ((int)buf[len] & 0xFF);
    return( len+1 );
  }

  // $B%*%Z%i%s%I5-9f(B o$B$N2r@O$r9T$&(B
  int _operand_1( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src.kind = Operand.IMM;
    dinfo.src.imm = 1;
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B o$B$N2r@O$r9T$&(B
  int _operand_C( byte buf[], int len ) {
    len = _amb_analyze( buf, len );
    dinfo.src.kind = Operand.REG;
    dinfo.src.reg_no = Cpu.CX;
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B I$B$N2r@O$r9T$&(B
  int _operand_I( byte buf[], int len ) {
    len = _imm_analyze( buf, len );
    dinfo.dst.init( );
    // $B%G%9%H%*%Z%i%s%I@_Dj(B
    dinfo.dst.kind = Operand.REG;
    dinfo.dst.reg_no = Cpu.AX;
    if( dinfo.r_flag ) {
      dinfo.dst.kind = Operand.REG;
      dinfo.dst.reg_no = dinfo.r_val;
    }
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B i$B$N2r@O$r9T$&(B
  int _operand_i( byte buf[], int len ) {
    dinfo.o16_flag = true;
    len = _imm_analyze( buf, len );
    dinfo.dst.init( );
    // $B%G%9%H%*%Z%i%s%I@_Dj(B
    dinfo.dst.kind = Operand.REG;
    dinfo.dst.reg_no = Cpu.AX;
    if( dinfo.r_flag ) {
      dinfo.dst.kind = Operand.REG;
      dinfo.dst.reg_no = dinfo.r_val;
    }
    return( len );
  }

  // $B%*%Z%i%s%I5-9f(B J$B$N2r@O$r9T$&(B
  int _operand_J( byte buf[], int len ) {
    len = _imm_analyze( buf, len );
    // $B%=!<%9%*%Z%i%s%I(B accum $B7hDj(B
    dinfo.dst.kind = Operand.REG;
    dinfo.dst.reg_no = Cpu.AX;
    return( len );
  }

  int _imm_analyze( byte buf[], int len ) {
    if( (dinfo.s_flag && ( dinfo.s_val != 0 ))
       ||
	(dinfo.w_flag && ( dinfo.w_val == 0 ))
  	 ) {
      dinfo.src.kind = Operand.IMM;
      dinfo.src.imm = (int)buf[len];
      len += 1;
    }
    else {
      if( dinfo.o16_flag ) {
        dinfo.src.kind = Operand.IMM; 
        dinfo.src.imm = (int)Util.to16( buf, len );
        len += 2;
      }
      else {
        dinfo.src.kind = Operand.IMM;
        dinfo.src.imm = Util.to32( buf, len );
        len += 4;
      }
    }
    return( len );
  }

  // $B%l%8%9%?%U!<%k%I$N2r@O(B
  void reg_analyze( Operand src, int reg ) {
    dinfo.src.reg_no = reg;
    dinfo.src.kind = Operand.REG;
    if(( dinfo.w_flag )&&(dinfo.w_val == 0)) {   dinfo.src.kind = Operand.HREG;  }
  }

  // $B%"%I%l%C%7%s%0%b!<%I%P%$%H$N2r@O(B
  // $B2r@O$7$?%P%$%H?t$rJV$9(B
  int _amb_analyze( byte buf[], int len ) {
    dinfo.mod = (buf[len] >> 6) & 0x3;
    dinfo.reg = (buf[len] >> 3) & 0x7;
    dinfo.rpm = buf[len] & 0x7;

    // $B%=!<%9%l%8%9%?(B
    reg_analyze( dinfo.src, dinfo.reg );

    // $B%G%#%9%H%M!<%7%g%s%a%b%j(B
    if( 3 == dinfo.mod ) {
      if(( dinfo.w_flag )&&(dinfo.w_val == 0)) {
        dinfo.dst.kind = Operand.HREG;
        dinfo.dst.reg_no = dinfo.rpm;
        len += 1;
      }
      else {
        dinfo.dst.kind = Operand.REG;
        dinfo.dst.reg_no = dinfo.rpm;
        len += 1;
      }
    }
    if( 2 == dinfo.mod ) {
      if( 4 == dinfo.rpm ) {
	len = _sib_analyze( buf, len+1, dinfo.mod );
	dinfo.dst.disp = Util.to32( buf, len );
	dinfo.dst.ea_disp_flag = true;
	len += 4;
      }
      else {
	dinfo.dst.kind = Operand.RREG;
	dinfo.dst.reg_no = dinfo.rpm;
	dinfo.dst.disp = Util.to32( buf, len+1 );
	len += 5;
      }
    }
    if( 1 == dinfo.mod ) {
      if( 4 == dinfo.rpm ) {
	len = _sib_analyze( buf, len+1, dinfo.mod );
	dinfo.dst.disp = (int)buf[len];
	dinfo.dst.ea_disp_flag = true;
	len += 1;
      }
      else {
	dinfo.dst.kind = Operand.RREG;
	dinfo.dst.reg_no = dinfo.rpm;
	dinfo.dst.disp = (int)buf[len+1];
	len += 2;
      }
    }
    if( 0 == dinfo.mod ) {
      if( 4 == dinfo.rpm ) {
	len = _sib_analyze( buf, len+1, dinfo.mod );
      }
      else {
	if( 5 == dinfo.rpm ) {
	  dinfo.dst.kind = Operand.MEM;
	  dinfo.dst.adrs = Util.to32( buf, len+1 );
	  len += 5;
	}
	else {
	  dinfo.dst.kind = Operand.RREG;
	  dinfo.dst.reg_no = dinfo.rpm;
	  len += 1;
	}
      }
    }
    return( len );
  }

  // $B%"%I%l%C%7%s%0%b!<%I%P%$%H$N2r@O(B
  int _sib_analyze( byte buf[], int len, int mod ) {
    dinfo.scale = (buf[len] >> 6) & 0x3;
    dinfo.index = (buf[len] >> 3) & 0x7;
    dinfo.base  =  buf[len] & 0x7;

    if( sysinfo.debug( )) {
      System.out.println(  " base =" + dinfo.base + " index =" + dinfo.index + " scale =" + dinfo.scale + " mod =" + dinfo.mod );
    }

    dinfo.dst.kind = Operand.EA;
    dinfo.dst.scale = 1 << dinfo.scale;
    dinfo.dst.base_is_reg = false;
    if( 5 == dinfo.base ) {
      if( mod == 0 ) {
        dinfo.dst.base_val = Util.to32( buf, len+1 );
        len += 5;
      }
      else {
	dinfo.dst.base_is_reg = true;
        dinfo.dst.base_reg = Cpu.BP;
        len += 1;
      }
    }
    else {
      dinfo.dst.base_is_reg = true;
      dinfo.dst.base_reg = dinfo.base;
      len += 1;
    }
    dinfo.dst.index_reg = dinfo.index;
    return( len );
  }

  // $BL?Na$r7hDj$9$k(B
  int _decide_inst_index( byte buf[] ) {
    int i;
    int len = 0;
    dinfo.o16_flag = false;
    dinfo.repnz_flag = false;
    dinfo.repz_flag = false;
    if( buf[len] == REPNZ )             { len += 1; dinfo.repnz_flag = true; }
    if( buf[len] == REPZ )              { len += 1; dinfo.repz_flag = true; }
    if( buf[len] == OperandSizePrefix ) { len += 1; dinfo.o16_flag = true; }

    if( -1 != ( i = inst_search_list[ (int)buf[len] & 0xFF ] )) {
      return( i );
    }
    for( i = 0 ; i < inst_num ; i++ ) {
      if( 
	 ( inst[i].opecode[0] == ( inst[i].mask[0] & buf[len] ))
	  &&
	 ( inst[i].opecode[1] == ( inst[i].mask[1] & buf[len+1] ))
	   ) {
	return( i );
      }
    }
    return( i );
  }
}

