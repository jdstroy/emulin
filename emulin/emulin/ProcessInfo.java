// ----------------------------------------
//  Emulin ProcessInfo
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 1999/04/05 16:30:21 $ 
//  $Id: KernelCore.java,v 1.2 1999/04/05 16:30:21 kiyoka Exp kiyoka $
// ----------------------------------------
package emulin;

import java.io.*;
import java.lang.*;
import emulin.*;

public class ProcessInfo {
  Process process;
  int ppid;
}
