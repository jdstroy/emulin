// ----------------------------------------
//  Process Memory Management
//
//  Copyright (C) 1999  Kiyoka Nishiyama
//
//  $Date: 2000/01/23 11:16:42 $ 
//  $Id: Memory.java,v 1.37 2000/01/23 11:16:42 kiyoka Exp $
// ----------------------------------------
package emulin;

import java.lang.*;
import java.io.*;
import java.util.*;
import emulin.*;

class AllocInfo {
  boolean use;
  int address;
  int size;
  int fd;
  int map_offset;
  int map_size;
  byte buf[];

  AllocInfo( ) {
    init( );
  }
  public void init( ) {
    use = false;
    address = 0;
    size = 0;
  }

  // $B<+J,$NJ#@=$rJV$9(B
  public AllocInfo duplicate( ) {
    AllocInfo _allocinfo = new AllocInfo( );
    _allocinfo.use        = use;
    _allocinfo.address    = address;
    _allocinfo.size       = size;
    _allocinfo.fd         = fd;
    _allocinfo.map_offset = map_offset;
    _allocinfo.map_size   = map_size;
    if( buf != null ) {
      _allocinfo.buf = new byte[buf.length];
      System.arraycopy( buf, 0, _allocinfo.buf, 0, buf.length );
    }
    return( _allocinfo );
  }
}

public class Memory extends Elf
{
  static int cache_size = 8;
  static int memory_page_size = 4096;
  static int memory_top = 0x40000000;
  Syscall syscall;
  int mark_address;
  Vector alloclist;
  int  cache_address;
  byte cache[];

  // $B=i4|2=(B
  Memory( Sysinfo _sysinfo, Syscall _syscall, Process _process ) {
    int i;
    sysinfo = _sysinfo;
    syscall = _syscall;
    process = _process;
    mark_address = memory_top;
    alloclist = new Vector( );
    cache = new byte[cache_size];
  }

  // $B<+J,$NJ#@=$rJV$9(B
  public Memory duplicate( Process _process ) {
    int i;
    Memory _memory = new Memory( sysinfo, _process.syscall, _process );
    _memory.mark_address = mark_address;
    _memory.alloclist = new Vector( );
    // $BA4$F$N%(%l%a%s%H$rJ#@=$9$k!#(B
    for( i = 0 ; i < alloclist.size( ) ; i++ ) { 
      AllocInfo allocinfo = (AllocInfo)alloclist.elementAt( i );
      if( null == allocinfo ) {
	_memory.alloclist.addElement( (Object)null );
      }
      else {
	_memory.alloclist.addElement( (Object)allocinfo.duplicate( ));
      }
    }
    _memory.update_info( (Elf)this );
    return( _memory );
  }

  // $B%a%b%j$r3NJ]$7$F%U%!%$%k$K%^%C%T%s%0$9$k!#(B
  public int alloc_and_map( int adrs, int size, int _fd, int offset ) {
    int address = alloc( adrs, size );
    AllocInfo allocinfo;
    allocinfo = (AllocInfo)alloclist.lastElement( );
    allocinfo.fd         = _fd;
    allocinfo.map_offset = offset;
    allocinfo.map_size   = size;
    if( sysinfo.verbose( )) {
      process.println( " alloc: fd = " + _fd + " address = " + Util.hexstr( address, 8 ) + " size = " + Util.hexstr( size, 8 ) + " offset = " + Util.hexstr( offset, 8 ) );
    }
    if( _fd > -1 ) {
      int ptr = allocinfo.map_offset;
      syscall.FileSeek( _fd, ptr, FileAccess.SEEK_SET );
      syscall.FileRead( _fd, allocinfo.buf );
      if( sysinfo.debug( )) {
	dump( address, size );
      }
    }
    return( address );
  }

  // $B%a%b%j3NJ]$9$k(B ( $B3NJ]$7$?%"%I%l%9$rJV$9(B )
  public int alloc( int adrs, int size ) {
    AllocInfo allocinfo = new AllocInfo( );
    int address = mark_address;
    int pages = 0;
    if( adrs != 0 ) {
      address = adrs;
      mark_address = adrs;
    }
    allocinfo.use     = true;
    allocinfo.address = address;
    allocinfo.size    = size;
    allocinfo.buf     = new byte[size];
    alloclist.addElement( (Object)allocinfo );
    pages = size / memory_page_size;
    if( pages == 0 ) pages++;
    mark_address += pages * memory_page_size;
    if( sysinfo.verbose( )) {
      process.println( " alloc( ) : address = " + Util.hexstr( address, 8 ) +  " next_address = " + Util.hexstr( mark_address, 8 ) + " pages = " + pages );
    }
    return( address );
  }

  public int realloc( int old_address, int size ) {
    int i;
    byte old_buf[];
    int  old_size;
    AllocInfo allocinfo;

    // $B4{$K3NJ]$5$l$F$$$k%a%b%j$rC5$7!"%a%b%j%5%$%:$rJQ99$9$k(B
    for( i = 0 ; i < alloclist.size( ) ; i++ ) {
      int j;
      allocinfo = (AllocInfo)alloclist.elementAt( i );
      if( old_address == allocinfo.address ) {
	// $B?7$7$$%a%b%j%P%C%U%!$r3NJ]$9$k!#(B
	old_buf  = allocinfo.buf;
	old_size = allocinfo.size;
	allocinfo.size    = size;
	allocinfo.buf     = new byte[size];
	// $B8E$$%G!<%?$r%3%T!<$9$k(B
	for( j = 0 ; j < old_size ; j++ ) {
	  allocinfo.buf[j] = old_buf[j];
	}
	return( 0 );
      }
    }
    return( -1 );
  }

  public int free( int address, int size ) {
    int i;
    AllocInfo allocinfo;
    for( i = 0 ; i < alloclist.size( ) ; i++ ) {
      allocinfo = (AllocInfo)alloclist.elementAt( i );
      if( (address == allocinfo.address)&&(size == allocinfo.size) ) {
	alloclist.removeElementAt( i );
	if( sysinfo.verbose( )) {
	  process.println( " free : address = " + Util.hexstr( address, 8 ) + " size = " + Util.hexstr( size, 8 ));
	}
	return( 0 );
      }
    }
    return( -1 );
  }

  // $B%"%I%l%9$,M-8z$J%a%b%jFb$+D4$Y$k(B
  public boolean in( int address ) {
    int i;
    boolean ret = false;
    for( i = 0 ; i < segment.length ; i++ ) {
      if( segment[i].in( address )) {
	ret = true;
	break;
      }
    }
    // $B%a%b%j3NJ]%"%I%l%9$+$i$ND4::(B
    if( !alloclist.isEmpty( )) {
      AllocInfo start_allocinfo = (AllocInfo)alloclist.firstElement( );
      AllocInfo end_allocinfo   = (AllocInfo)alloclist.lastElement( );
      if( (start_allocinfo.address <= address) &&
	  ( address < (end_allocinfo.address + end_allocinfo.size))) {
	ret = true;
      }
    }
    return( ret );
  }

  // $B%a%b%j$+$i$N(B1$B%P%$%H%j!<%I(B
  byte load8( int address ) {
    int i;
    boolean _in = false;
    int align_address = (address / cache_size) * cache_size;
    if( cache_address != align_address ) {
      cache_address = align_address;
      // $B%-%c%C%7%e$X$N%j!<%I(B
      for( i = 0 ; i < segment.length ; i++ ) {
	if( segment[i].in( address )) {
	  segment[i].peekbs( align_address, cache );
	  _in = true;
	  break;
	}
      }
      if( !_in ) {
	for( i = 0 ; i < alloclist.size( ) ; i++ ) {
	  AllocInfo allocinfo = (AllocInfo)alloclist.elementAt( i );
	  int adrs            = allocinfo.address;
	  int size            = allocinfo.size;
	  int align_index     = align_address - adrs;
	  if( ( adrs <= address            ) && ( address            < (adrs + size))) {
	    int j;
	    // $B%U%!%$%k$,%^%C%W$5$l$F$$$J$$$?$@$N%a%b%j$N>l9g(B
	    for( j = 0 ; j < cache_size ; j++ ) {
	      cache[j] = 0;
	      if( align_index+j < size ) { cache[j] = allocinfo.buf[ align_index+j ]; }
	    }
	    _in = true;
	  }
	}
	if( ! _in ) {
	  process.println( "  Segmentation Fault address(load8) = : " + Util.hexstr( address, 8 ) );
	  process.println( "  Segmentation Fault address(load8) :   evals = " + process.evals( ));
	  System.exit( 1 );
	}
      }
      //    if( sysinfo.debug( )) {
      //      process.println( "  Load8(" + Util.hexstr( address, 8 ) + ") = [" + Util.hexstr( ret & 0xFF, 2 ) + "] " );
      //    }
    }
    return( cache[address - cache_address] );
  }


  // $B%a%b%j$K(B1$B%P%$%H$N%G!<%?$r=q$-9~$`(B
  public boolean store8( int address, int data ) {
    int i;
    boolean ret   = false;
    cache_address = 0; // $B%-%c%C%7%e$NGK4~(B
    for( i = 0 ; i < segment.length ; i++ ) {
      if( segment[i].in( address )) {
	segment[i].pokeb( address, (byte)data );
	ret = true;
	break;
      }
    }
    if( !ret ) {
      for( i = 0 ; i < alloclist.size( ) ; i++ ) {
	AllocInfo allocinfo = (AllocInfo)alloclist.elementAt( i );
	int adrs = allocinfo.address;
	int size = allocinfo.size;
	int fd   = allocinfo.fd;
	if( ( adrs <= address) &&
	    ( address < (adrs + size))) {
	  if( -1 == fd ) {
	    // $B%U%!%$%k$,%^%C%W$5$l$F$$$J$$$?$@$N%a%b%j$N>l9g(B
	    allocinfo.buf[ address - adrs ] = (byte)data;
	    ret = true;
	  }
	  else {
	    // $B%U%!%$%k$,%^%C%W$5$l$F$$$k>l9g(B
	    allocinfo.buf[ address - adrs ] = (byte)data;
	    if( false ) {
	      process.println( "  Warning : Emulin    memory mapped file store is Unsupport... fd = " +
				  fd + "  address = " + Util.hexstr( address, 8 ) );
	    }
	    ret = true;
	  }
	}
      }
      if( !ret ) {
	process.println( "  Segmentation Fault address(store8) = : " + Util.hexstr( address, 8 ) );
	process.println( "  Segmentation Fault address(load8) :   evals = " + process.evals( ));
	System.exit( 1 );
      }
    }
    if( sysinfo.debug( )) {
      process.println( "  Store8(" + Util.hexstr( address, 8 ) + "," + Util.hexstr( data & 0xFF, 2 ) + ") " );
    }
    return( ret );
  }

  //---------------------------------------- $B0J2<1~MQ4X?t(B ----------------------------------------
  // $B%a%b%j$+$i$N%G!<%?%j!<%I(B
  public boolean fetch( int address, byte buf[] ) {
    int i;
    for( i = 0 ; i < buf.length ; i++ ) {
      buf[i] = load8( address + i );
    }
    return( true );
  }

  // $B%a%b%j$+$i$N(B2$B%P%$%H%j!<%I(B
  public short load16( int address ) {
    short ret;
    ret = (short) ( ((int)load8( address ) & 0xFF) | (((int)load8( address+1 ) & 0xFF) << 8));
    //    if( sysinfo.debug( )) {
    //      process.println( "  Load16(" + Util.hexstr( address, 8 ) + ") = [" + Util.hexstr( ret & 0xFFFF, 4 ) + "] " );
    //    }
    return( ret );
  }

  // $B%a%b%j$+$i$N(B4$B%P%$%H%j!<%I(B
  public int load32( int address ) {
    int ret;
    ret =  
	   (int)
	   ( ((int)load8( address ) & 0xFF) |
	     (((int)load8( address+1 ) & 0xFF) << 8 ) |
	     (((int)load8( address+2 ) & 0xFF) << 16) |
	     (((int)load8( address+3 ) & 0xFF) << 24) 
	     );
    if( sysinfo.debug( )) {
      process.println( "  Load32(" + Util.hexstr( address, 8 ) + ") = [" + Util.hexstr( ret, 8 ) + "] " );
    }
    return( ret );
  }

  // $B%a%b%j$+$i$N(B8$B%P%$%H%j!<%I(B
  public long load64( int address ) {
    long ret;
    ret =  
	   (long)
	   ( ((long)load8( address+0 ) & 0xFFL) |
	     (((long)load8( address+1 ) & 0xFFL) << 8 ) |
	     (((long)load8( address+2 ) & 0xFFL) << 16) |
	     (((long)load8( address+3 ) & 0xFFL) << 24) |
	     (((long)load8( address+4 ) & 0xFFL) << 32) |
	     (((long)load8( address+5 ) & 0xFFL) << (32 +  8)) |
	     (((long)load8( address+6 ) & 0xFFL) << (32 + 16)) |
	     (((long)load8( address+7 ) & 0xFFL) << (32 + 24))
	     );
    if( sysinfo.debug( )) {
      process.println( "  Load64(" + Util.hexstr( address, 8 ) + " )  = [ " + Util.hexstr( (int)(ret>>32), 8 ) + Util.hexstr( (int)ret, 8 ) + " ] " );
    }
    return( ret );
  }

  // $B%a%b%j$X$N(B2$B%P%$%H%i%$%H(B
  public void store16( int address, short value ) {
    store8( address+0,  value        & 0xFF );
    store8( address+1, (value >> 8 ) & 0xFF );
    //    if( sysinfo.debug( )) {
    //      process.println( "  Store16(" + Util.hexstr( address, 8 ) + "," + Util.hexstr( value & 0xFFFF, 4 ) + ") " );
    //    }
  }

  // $B%a%b%j$X$N(B4$B%P%$%H%i%$%H(B
  public void store32( int address, int value ) {
    store8( address+0,  value        & 0xFF );
    store8( address+1, (value >>  8) & 0xFF );
    store8( address+2, (value >> 16) & 0xFF );
    store8( address+3, (value >> 24) & 0xFF );
    //    if( sysinfo.debug( )) {
    //      process.println( "  Store32(" + Util.hexstr( address, 8 ) + "," + Util.hexstr( value, 8 ) + ") " );
    //    }
  }

  // $B%a%b%j$X$N(B8$B%P%$%H%i%$%H(B
  public void store64( int address, long value ) {
    store8( address+0, (int)(value >>  0   ) & 0xFF );
    store8( address+1, (int)(value >>  8   ) & 0xFF );
    store8( address+2, (int)(value >> 16   ) & 0xFF );
    store8( address+3, (int)(value >> 24   ) & 0xFF );
    store8( address+4, (int)(value >> 32+ 0) & 0xFF );
    store8( address+5, (int)(value >> 32+ 8) & 0xFF );
    store8( address+6, (int)(value >> 32+16) & 0xFF );
    store8( address+7, (int)(value >> 32+24) & 0xFF );
  }

  // $BJ8;zNs$r3JG<$9$k(B
  public int storeString( int address, String str ) {
    int i;
    for( i = 0 ; i < str.length( ) ; i++ ) {
      store8( address, (byte)str.charAt( i ));
      address ++;
    }
    store8( address, 0 );
    address ++;
    return( address );
  }

  // $BJ8;zNs$rFI$_=P$9(B
  public String loadString( int address ) {
    int len, i;
    char buf[];
    String ret = "";
    for( i = 0 ; i < 10000 ; i++ ) {
      if(0 == (char)load8( address+i )) { break; }
    }
    len = i;
    buf = new char[len];
    for( i = 0 ; i < len ; i++ ) {
      buf[i] = (char)load8( address+i );
    }
    return( ret.copyValueOf( buf ));
  }

  // DUMP$B$r$H$k(B
  public void dump( int address, int len ) {
    int i, j, index = 0;
    String str;

    if( false ) {
      return;
    }
    else {
      if( len < 16 ) { len = 16; }
      for( i = 0 ; i < len/16 ; i++ ) {
	str = " " + Util.hexstr( address + index, 8 ) + ": ";
	for( j = 0 ; j < 16 ; j++ ) {
	  if( in( address + index )) {
	    str += " " + Util.hexstr( 0xFF & (int)load8( address + index ), 2 );
	    index ++;
	  }
	}
	process.println( str );
      }
    }
  }
}

